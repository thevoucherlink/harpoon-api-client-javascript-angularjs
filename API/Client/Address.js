goog.provide('API.Client.Address');

/**
 * @record
 */
API.Client.Address = function() {}

/**
 * Main street line for the address (first line)
 * @type {!string}
 * @export
 */
API.Client.Address.prototype.streetAddress;

/**
 * Line to complete the address (second line)
 * @type {!string}
 * @export
 */
API.Client.Address.prototype.streetAddressComp;

/**
 * @type {!string}
 * @export
 */
API.Client.Address.prototype.city;

/**
 * Region, County, Province or State
 * @type {!string}
 * @export
 */
API.Client.Address.prototype.region;

/**
 * Country ISO code (must be max of 2 capital letter)
 * @type {!string}
 * @export
 */
API.Client.Address.prototype.countryCode;

/**
 * @type {!string}
 * @export
 */
API.Client.Address.prototype.postcode;

/**
 * Postal Attn {person first name}
 * @type {!string}
 * @export
 */
API.Client.Address.prototype.attnFirstName;

/**
 * Postal Attn {person last name}
 * @type {!string}
 * @export
 */
API.Client.Address.prototype.attnLastName;

/**
 * @type {!number}
 * @export
 */
API.Client.Address.prototype.id;

