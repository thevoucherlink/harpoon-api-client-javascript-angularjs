goog.provide('API.Client.Analytic');

/**
 * @record
 */
API.Client.Analytic = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.Analytic.prototype.id;

