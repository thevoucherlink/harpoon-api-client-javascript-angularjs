goog.provide('API.Client.AnalyticRequest');

/**
 * @record
 */
API.Client.AnalyticRequest = function() {}

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.AnalyticRequest.prototype.requests;

/**
 * @type {!string}
 * @export
 */
API.Client.AnalyticRequest.prototype.scope;

/**
 * @type {!number}
 * @export
 */
API.Client.AnalyticRequest.prototype.id;

/** @enum {string} */
API.Client.AnalyticRequest.ScopeEnum = { 
  inapp: 'inapp',
  beacon: 'beacon',
}
