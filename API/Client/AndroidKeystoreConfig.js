goog.provide('API.Client.AndroidKeystoreConfig');

/**
 * @record
 */
API.Client.AndroidKeystoreConfig = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.AndroidKeystoreConfig.prototype.storePassword;

/**
 * @type {!string}
 * @export
 */
API.Client.AndroidKeystoreConfig.prototype.keyAlias;

/**
 * @type {!string}
 * @export
 */
API.Client.AndroidKeystoreConfig.prototype.keyPassword;

/**
 * @type {!string}
 * @export
 */
API.Client.AndroidKeystoreConfig.prototype.hashKey;

/**
 * @type {!number}
 * @export
 */
API.Client.AndroidKeystoreConfig.prototype.id;

