goog.provide('API.Client.AnonymousModel_8');

/**
 * @record
 */
API.Client.AnonymousModel8 = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.AnonymousModel8.prototype.brandId;

/**
 * @type {!number}
 * @export
 */
API.Client.AnonymousModel8.prototype.competitionId;

/**
 * @type {!number}
 * @export
 */
API.Client.AnonymousModel8.prototype.couponId;

/**
 * @type {!number}
 * @export
 */
API.Client.AnonymousModel8.prototype.customerId;

/**
 * @type {!number}
 * @export
 */
API.Client.AnonymousModel8.prototype.eventId;

/**
 * @type {!number}
 * @export
 */
API.Client.AnonymousModel8.prototype.dealPaid;

/**
 * @type {!number}
 * @export
 */
API.Client.AnonymousModel8.prototype.dealGroup;

