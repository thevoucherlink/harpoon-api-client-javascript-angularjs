goog.provide('API.Client.App');

/**
 * @record
 */
API.Client.App = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.App.prototype.id;

/**
 * Id used internally in Harpoon
 * @type {!number}
 * @export
 */
API.Client.App.prototype.internalId;

/**
 * Vendor App owner
 * @type {!number}
 * @export
 */
API.Client.App.prototype.vendorId;

/**
 * Name of the application
 * @type {!string}
 * @export
 */
API.Client.App.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.App.prototype.category;

/**
 * @type {!string}
 * @export
 */
API.Client.App.prototype.description;

/**
 * @type {!string}
 * @export
 */
API.Client.App.prototype.keywords;

/**
 * @type {!string}
 * @export
 */
API.Client.App.prototype.copyright;

/**
 * @type {!string}
 * @export
 */
API.Client.App.prototype.realm;

/**
 * Bundle Identifier of the application, com.{company name}.{project name}
 * @type {!string}
 * @export
 */
API.Client.App.prototype.bundle;

/**
 * URL Scheme used for deeplinking
 * @type {!string}
 * @export
 */
API.Client.App.prototype.urlScheme;

/**
 * Customer need to follow the Brand to display the Offers outside of the Brand profile page
 * @type {!boolean}
 * @export
 */
API.Client.App.prototype.brandFollowOffer;

/**
 * URL linking to the Privacy page for the app
 * @type {!string}
 * @export
 */
API.Client.App.prototype.privacyUrl;

/**
 * URL linking to the Terms of Service page for the app
 * @type {!string}
 * @export
 */
API.Client.App.prototype.termsOfServiceUrl;

/**
 * Restrict the use of the app to just the customer with at least the value specified, 0 = no limit
 * @type {!number}
 * @export
 */
API.Client.App.prototype.restrictionAge;

/**
 * URL linking to the Google Play page for the app
 * @type {!string}
 * @export
 */
API.Client.App.prototype.storeAndroidUrl;

/**
 * URL linking to the iTunes App Store page for the app
 * @type {!string}
 * @export
 */
API.Client.App.prototype.storeIosUrl;

/**
 * News Feed source, nativeFeed/rss
 * @type {!string}
 * @export
 */
API.Client.App.prototype.typeNewsFeed;

/**
 * @type {!string}
 * @export
 */
API.Client.App.prototype.clientSecret;

/**
 * @type {!Array<!string>}
 * @export
 */
API.Client.App.prototype.grantTypes;

/**
 * @type {!Array<!string>}
 * @export
 */
API.Client.App.prototype.scopes;

/**
 * Status of the application, production/sandbox/disabled
 * @type {!string}
 * @export
 */
API.Client.App.prototype.status;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.App.prototype.created;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.App.prototype.modified;

/**
 * Size of the Offer List Item, big/small
 * @type {!string}
 * @export
 */
API.Client.App.prototype.styleOfferList;

/**
 * @type {!API.Client.AppConfig}
 * @export
 */
API.Client.App.prototype.appConfig;

/**
 * @type {!string}
 * @export
 */
API.Client.App.prototype.clientToken;

/**
 * @type {!string}
 * @export
 */
API.Client.App.prototype.multiConfigTitle;

/**
 * @type {!Array<!API.Client.AppConfig>}
 * @export
 */
API.Client.App.prototype.multiConfig;

/**
 * @type {!string}
 * @export
 */
API.Client.App.prototype.appId;

/**
 * @type {!string}
 * @export
 */
API.Client.App.prototype.parentId;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.App.prototype.rssFeeds;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.App.prototype.rssFeedGroups;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.App.prototype.radioStreams;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.App.prototype.customers;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.App.prototype.appConfigs;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.App.prototype.playlists;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.App.prototype.apps;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.App.prototype.parent;

/** @enum {string} */
API.Client.App.TypeNewsFeedEnum = { 
  nativeFeed: 'nativeFeed',
  rss: 'rss',
}
/** @enum {string} */
API.Client.App.StatusEnum = { 
  production: 'production',
  sandbox: 'sandbox',
  disabled: 'disabled',
}
/** @enum {string} */
API.Client.App.StyleOfferListEnum = { 
  big: 'big',
  small: 'small',
}
