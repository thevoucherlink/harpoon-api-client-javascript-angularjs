goog.provide('API.Client.AppAd');

/**
 * @record
 */
API.Client.AppAd = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.AppAd.prototype.unitId;

/**
 * @type {!string}
 * @export
 */
API.Client.AppAd.prototype.unitName;

/**
 * @type {!string}
 * @export
 */
API.Client.AppAd.prototype.unitType;

/** @enum {string} */
API.Client.AppAd.UnitTypeEnum = { 
  banner: 'banner',
  fullscreen: 'fullscreen',
  nativeAd: 'nativeAd',
  rewardVideo: 'rewardVideo',
  mRect: 'mRect',
  custom: 'custom',
}
