goog.provide('API.Client.AppColor');

/**
 * @record
 */
API.Client.AppColor = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.AppColor.prototype.appPrimary;

/**
 * @type {!string}
 * @export
 */
API.Client.AppColor.prototype.appSecondary;

/**
 * @type {!string}
 * @export
 */
API.Client.AppColor.prototype.textPrimary;

/**
 * @type {!string}
 * @export
 */
API.Client.AppColor.prototype.textSecondary;

/**
 * @type {!string}
 * @export
 */
API.Client.AppColor.prototype.radioNavBar;

/**
 * @type {!string}
 * @export
 */
API.Client.AppColor.prototype.radioPlayer;

/**
 * @type {!string}
 * @export
 */
API.Client.AppColor.prototype.radioProgressBar;

/**
 * @type {!string}
 * @export
 */
API.Client.AppColor.prototype.menuBackground;

/**
 * @type {!string}
 * @export
 */
API.Client.AppColor.prototype.radioTextSecondary;

/**
 * @type {!string}
 * @export
 */
API.Client.AppColor.prototype.radioTextPrimary;

/**
 * @type {!string}
 * @export
 */
API.Client.AppColor.prototype.menuItem;

/**
 * @type {!string}
 * @export
 */
API.Client.AppColor.prototype.appGradientPrimary;

/**
 * @type {!string}
 * @export
 */
API.Client.AppColor.prototype.feedSegmentIndicator;

/**
 * @type {!string}
 * @export
 */
API.Client.AppColor.prototype.radioPlayButton;

/**
 * @type {!string}
 * @export
 */
API.Client.AppColor.prototype.radioPauseButton;

/**
 * @type {!string}
 * @export
 */
API.Client.AppColor.prototype.radioPlayButtonBackground;

/**
 * @type {!string}
 * @export
 */
API.Client.AppColor.prototype.radioPauseButtonBackground;

/**
 * @type {!string}
 * @export
 */
API.Client.AppColor.prototype.radioPlayerItem;

