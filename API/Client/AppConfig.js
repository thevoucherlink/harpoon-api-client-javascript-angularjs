goog.provide('API.Client.AppConfig');

/**
 * @record
 */
API.Client.AppConfig = function() {}

/**
 * App logo
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.imgAppLogo;

/**
 * App splash-screen
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.imgSplashScreen;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.imgBrandLogo;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.imgNavBarLogo;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.imgBrandPlaceholder;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.imgMenuBackground;

/**
 * Enable or disable the Radio feature within the app
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureRadio;

/**
 * Enable or disable the Radio autoplay feature within the app
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureRadioAutoPlay;

/**
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureNews;

/**
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureEvents;

/**
 * If true the Events list is categorised
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureEventsCategorized;

/**
 * If true the user will have the possibility to see Events' attendees
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureEventsAttendeeList;

/**
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureCommunity;

/**
 * If true the Brand list is categories
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureCommunityCategorized;

/**
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureCompetitions;

/**
 * If true the user will have the possibility to see Competitions' attendees
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureCompetitionsAttendeeList;

/**
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureOffers;

/**
 * If true the Offer list is categories
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureOffersCategorized;

/**
 * If true the user will have the possibility to follow or unfollow Brands
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureBrandFollow;

/**
 * If true the user will have the possibility to see Brands' followers
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureBrandFollowerList;

/**
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureWordpressConnect;

/**
 * If true the user will be asked to login before getting access to the app
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureLogin;

/**
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureHarpoonLogin;

/**
 * If true the user will have the possibility to login with Facebook
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureFacebookLogin;

/**
 * If true the user will need to verify its account using a phone number
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featurePhoneVerification;

/**
 * Show / Hide media player
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featurePlayer;

/**
 * Show / Hide main button
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featurePlayerMainButton;

/**
 * If true the user actions will be tracked while using the app
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureGoogleAnalytics;

/**
 * Media Player Main Button title
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.playerMainButtonTitle;

/**
 * Media Player config
 * @type {!API.Client.PlayerConfig}
 * @export
 */
API.Client.AppConfig.prototype.playerConfig;

/**
 * @type {!Array<!API.Client.MenuItem>}
 * @export
 */
API.Client.AppConfig.prototype.menu;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.color;

/**
 * @type {!API.Client.AppColor}
 * @export
 */
API.Client.AppConfig.prototype.colors;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.googleAppId;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.iOSAppId;

/**
 * @type {!API.Client.GoogleAnalyticsTracking}
 * @export
 */
API.Client.AppConfig.prototype.googleAnalyticsTracking;

/**
 * @type {!API.Client.LoginExternal}
 * @export
 */
API.Client.AppConfig.prototype.loginExternal;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.rssWebViewCss;

/**
 * @type {!Array<!API.Client.AppAd>}
 * @export
 */
API.Client.AppConfig.prototype.ads;

/**
 * @type {!Array<!API.Client.AppAd>}
 * @export
 */
API.Client.AppConfig.prototype.iOSAds;

/**
 * @type {!Array<!API.Client.AppAd>}
 * @export
 */
API.Client.AppConfig.prototype.androidAds;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.pushNotificationProvider;

/**
 * @type {!API.Client.BatchConfig}
 * @export
 */
API.Client.AppConfig.prototype.batchConfig;

/**
 * @type {!API.Client.UrbanAirshipConfig}
 * @export
 */
API.Client.AppConfig.prototype.urbanAirshipConfig;

/**
 * @type {!API.Client.PushWooshConfig}
 * @export
 */
API.Client.AppConfig.prototype.pushWooshConfig;

/**
 * @type {!API.Client.FacebookConfig}
 * @export
 */
API.Client.AppConfig.prototype.facebookConfig;

/**
 * @type {!API.Client.TwitterConfig}
 * @export
 */
API.Client.AppConfig.prototype.twitterConfig;

/**
 * @type {!API.Client.RssTags}
 * @export
 */
API.Client.AppConfig.prototype.rssTags;

/**
 * @type {!API.Client.AppConnectTo}
 * @export
 */
API.Client.AppConfig.prototype.connectTo;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.iOSVersion;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.iOSBuild;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.iOSGoogleServices;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.iOSTeamId;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.iOSAppleId;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.iOSPushNotificationPrivateKey;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.iOSPushNotificationCertificate;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.iOSPushNotificationPem;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.iOSPushNotificationPassword;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.iOSReleaseNotes;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.androidBuild;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.androidVersion;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.androidGoogleServices;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.androidGoogleApiKey;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.androidGoogleCloudMessagingSenderId;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.androidKey;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.androidKeystore;

/**
 * @type {!API.Client.AndroidKeystoreConfig}
 * @export
 */
API.Client.AppConfig.prototype.androidKeystoreConfig;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.androidApk;

/**
 * @type {!Array<!API.Client.FormSection>}
 * @export
 */
API.Client.AppConfig.prototype.customerDetailsForm;

/**
 * @type {!API.Client.Contact}
 * @export
 */
API.Client.AppConfig.prototype.contact;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.imgSponsorMenu;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.imgSponsorSplash;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.audioSponsorPreRollAd;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.name;

/**
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.isChildConfig;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.typeNewsFocusSource;

/**
 * @type {!number}
 * @export
 */
API.Client.AppConfig.prototype.sponsorSplashImgDisplayTime;

/**
 * @type {!number}
 * @export
 */
API.Client.AppConfig.prototype.radioSessionInterval;

/**
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureRadioSession;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.wordpressShareUrlStub;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.iosUriScheme;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.androidUriScheme;

/**
 * @type {!API.Client.BranchConfig}
 * @export
 */
API.Client.AppConfig.prototype.branchConfig;

/**
 * @type {!number}
 * @export
 */
API.Client.AppConfig.prototype.defaultRadioStreamId;

/**
 * Enable or disable the Triton Player in App
 * @type {!boolean}
 * @export
 */
API.Client.AppConfig.prototype.featureTritonPlayer;

/**
 * @type {!number}
 * @export
 */
API.Client.AppConfig.prototype.id;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConfig.prototype.appId;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.AppConfig.prototype.app;

/** @enum {string} */
API.Client.AppConfig.PushNotificationProviderEnum = { 
  none: 'none',
  batch: 'batch',
  urbanairship: 'urbanairship',
  pushwoosh: 'pushwoosh',
}
/** @enum {string} */
API.Client.AppConfig.TypeNewsFocusSourceEnum = { 
  linkTag: 'linkTag',
  rssContent: 'rssContent',
  rssContentWithFeatureImage: 'rssContentWithFeatureImage',
}
