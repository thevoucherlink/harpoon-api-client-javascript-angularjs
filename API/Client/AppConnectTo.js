goog.provide('API.Client.AppConnectTo');

/**
 * @record
 */
API.Client.AppConnectTo = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.AppConnectTo.prototype.token;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConnectTo.prototype.heading;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConnectTo.prototype.description;

/**
 * @type {!string}
 * @export
 */
API.Client.AppConnectTo.prototype.link;

