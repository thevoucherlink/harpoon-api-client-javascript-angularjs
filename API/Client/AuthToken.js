goog.provide('API.Client.AuthToken');

/**
 * @record
 */
API.Client.AuthToken = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.AuthToken.prototype.accessToken;

/**
 * @type {!number}
 * @export
 */
API.Client.AuthToken.prototype.expiresIn;

/**
 * @type {!string}
 * @export
 */
API.Client.AuthToken.prototype.scope;

/**
 * @type {!string}
 * @export
 */
API.Client.AuthToken.prototype.refreshToken;

/**
 * @type {!string}
 * @export
 */
API.Client.AuthToken.prototype.tokenType;

/**
 * @type {!string}
 * @export
 */
API.Client.AuthToken.prototype.kid;

/**
 * @type {!string}
 * @export
 */
API.Client.AuthToken.prototype.macAlgorithm;

/**
 * @type {!string}
 * @export
 */
API.Client.AuthToken.prototype.macKey;

