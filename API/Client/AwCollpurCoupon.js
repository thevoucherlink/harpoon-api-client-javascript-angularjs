goog.provide('API.Client.AwCollpurCoupon');

/**
 * @record
 */
API.Client.AwCollpurCoupon = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurCoupon.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurCoupon.prototype.dealId;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurCoupon.prototype.purchaseId;

/**
 * @type {!string}
 * @export
 */
API.Client.AwCollpurCoupon.prototype.couponCode;

/**
 * @type {!string}
 * @export
 */
API.Client.AwCollpurCoupon.prototype.status;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.AwCollpurCoupon.prototype.couponDeliveryDatetime;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.AwCollpurCoupon.prototype.couponDateUpdated;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.AwCollpurCoupon.prototype.redeemedAt;

/**
 * @type {!string}
 * @export
 */
API.Client.AwCollpurCoupon.prototype.redeemedByTerminal;

