goog.provide('API.Client.AwCollpurDeal');

/**
 * @record
 */
API.Client.AwCollpurDeal = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDeal.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDeal.prototype.productId;

/**
 * @type {!string}
 * @export
 */
API.Client.AwCollpurDeal.prototype.productName;

/**
 * @type {!string}
 * @export
 */
API.Client.AwCollpurDeal.prototype.storeIds;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDeal.prototype.isActive;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDeal.prototype.isSuccess;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDeal.prototype.closeState;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDeal.prototype.qtyToReachDeal;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDeal.prototype.purchasesLeft;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDeal.prototype.maximumAllowedPurchases;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.AwCollpurDeal.prototype.availableFrom;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.AwCollpurDeal.prototype.availableTo;

/**
 * @type {!string}
 * @export
 */
API.Client.AwCollpurDeal.prototype.price;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDeal.prototype.autoClose;

/**
 * @type {!string}
 * @export
 */
API.Client.AwCollpurDeal.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.AwCollpurDeal.prototype.description;

/**
 * @type {!string}
 * @export
 */
API.Client.AwCollpurDeal.prototype.fullDescription;

/**
 * @type {!string}
 * @export
 */
API.Client.AwCollpurDeal.prototype.dealImage;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDeal.prototype.isFeatured;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDeal.prototype.enableCoupons;

/**
 * @type {!string}
 * @export
 */
API.Client.AwCollpurDeal.prototype.couponPrefix;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDeal.prototype.couponExpireAfterDays;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDeal.prototype.expiredFlag;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDeal.prototype.sentBeforeFlag;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDeal.prototype.isSuccessedFlag;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.AwCollpurDeal.prototype.awCollpurDealPurchases;

