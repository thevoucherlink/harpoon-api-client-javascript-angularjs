goog.provide('API.Client.AwCollpurDealPurchases');

/**
 * @record
 */
API.Client.AwCollpurDealPurchases = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDealPurchases.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDealPurchases.prototype.dealId;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDealPurchases.prototype.orderId;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDealPurchases.prototype.orderItemId;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDealPurchases.prototype.qtyPurchased;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDealPurchases.prototype.qtyWithCoupons;

/**
 * @type {!string}
 * @export
 */
API.Client.AwCollpurDealPurchases.prototype.customerName;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDealPurchases.prototype.customerId;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.AwCollpurDealPurchases.prototype.purchaseDateTime;

/**
 * @type {!string}
 * @export
 */
API.Client.AwCollpurDealPurchases.prototype.shippingAmount;

/**
 * @type {!string}
 * @export
 */
API.Client.AwCollpurDealPurchases.prototype.qtyOrdered;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDealPurchases.prototype.refundState;

/**
 * @type {!number}
 * @export
 */
API.Client.AwCollpurDealPurchases.prototype.isSuccessedFlag;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.AwCollpurDealPurchases.prototype.awCollpurCoupon;

