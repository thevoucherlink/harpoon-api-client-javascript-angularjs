/**
 * @fileoverview AUTOMATICALLY GENERATED service for API.Client.AwCollpurDealPurchasesApi.
 * Do not edit this file by hand or your changes will be lost next time it is
 * generated.
 *
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at &lt;a href&#x3D;&#39;https://harpoonconnect.com&#39;&gt;https://harpoonconnect.com&lt;/a&gt;, #harpoonConnect.
 * Version: 1.1.1
 * Generated by: class io.swagger.codegen.languages.JavascriptClosureAngularClientCodegen
 */
goog.provide('API.Client.AwCollpurDealPurchasesApi');

goog.require('API.Client.AwCollpurCoupon');
goog.require('API.Client.AwCollpurDealPurchases');
goog.require('API.Client.inline_response_200_1');
goog.require('API.Client.inline_response_200_2');
goog.require('API.Client.inline_response_200_3');

/**
 * @constructor
 * @param {!angular.$http} $http
 * @param {!Object} $httpParamSerializer
 * @param {!angular.$injector} $injector
 * @struct
 */
API.Client.AwCollpurDealPurchasesApi = function($http, $httpParamSerializer, $injector) {
  /** @private {!string} */
  this.basePath_ = $injector.has('AwCollpurDealPurchasesApiBasePath') ?
                   /** @type {!string} */ ($injector.get('AwCollpurDealPurchasesApiBasePath')) :
                   'https://apicdn.harpoonconnect.com/v1.1.1';

  /** @private {!Object<string, string>} */
  this.defaultHeaders_ = $injector.has('AwCollpurDealPurchasesApiDefaultHeaders') ?
                   /** @type {!Object<string, string>} */ (
                       $injector.get('AwCollpurDealPurchasesApiDefaultHeaders')) :
                   {};

  /** @private {!angular.$http} */
  this.http_ = $http;

  /** @package {!Object} */
  this.httpParamSerializer = $injector.get('$httpParamSerializer');
}
API.Client.AwCollpurDealPurchasesApi.$inject = ['$http', '$httpParamSerializer', '$injector'];

/**
 * Count instances of the model matched by where from the data source.
 * 
 * @param {!string=} opt_where Criteria to match model instances
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.inline_response_200_1>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesCount = function(opt_where, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases/count';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  if (opt_where !== undefined) {
    queryParameters['where'] = opt_where;
  }

  /** @type {!Object} */
  var httpRequestParams = {
    method: 'GET',
    url: path,
    json: true,
            params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Create a new instance of the model and persist it into the data source.
 * 
 * @param {!AwCollpurDealPurchases=} opt_data Model instance data
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.AwCollpurDealPurchases>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesCreate = function(opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'POST',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Create a change stream.
 * 
 * @param {!string=} opt_options 
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!Object>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream = function(opt_options, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases/change-stream';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  if (opt_options !== undefined) {
    queryParameters['options'] = opt_options;
  }

  /** @type {!Object} */
  var httpRequestParams = {
    method: 'GET',
    url: path,
    json: true,
            params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Create a change stream.
 * 
 * @param {!string=} opt_options 
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!Object>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream = function(opt_options, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases/change-stream';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  /** @type {!Object} */
  var formParams = {};

  headerParams['Content-Type'] = 'application/x-www-form-urlencoded';

  formParams['options'] = opt_options;

  /** @type {!Object} */
  var httpRequestParams = {
    method: 'POST',
    url: path,
    json: false,
        data: this.httpParamSerializer(formParams),
    params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Delete a model instance by {{id}} from the data source.
 * 
 * @param {!string} id Model id
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.Object>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesDeleteById = function(id, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases/{id}'
      .replace('{' + 'id' + '}', String(id));

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  // verify required parameter 'id' is set
  if (!id) {
    throw new Error('Missing required parameter id when calling awCollpurDealPurchasesDeleteById');
  }
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'DELETE',
    url: path,
    json: true,
            params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Check whether a model instance exists in the data source.
 * 
 * @param {!string} id Model id
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.inline_response_200_3>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists = function(id, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases/{id}/exists'
      .replace('{' + 'id' + '}', String(id));

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  // verify required parameter 'id' is set
  if (!id) {
    throw new Error('Missing required parameter id when calling awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists');
  }
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'GET',
    url: path,
    json: true,
            params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Check whether a model instance exists in the data source.
 * 
 * @param {!string} id Model id
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.inline_response_200_3>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid = function(id, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases/{id}'
      .replace('{' + 'id' + '}', String(id));

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  // verify required parameter 'id' is set
  if (!id) {
    throw new Error('Missing required parameter id when calling awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid');
  }
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'HEAD',
    url: path,
    json: true,
            params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Find all instances of the model matched by filter from the data source.
 * 
 * @param {!string=} opt_filter Filter defining fields, where, include, order, offset, and limit
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!Array<!API.Client.AwCollpurDealPurchases>>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesFind = function(opt_filter, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  if (opt_filter !== undefined) {
    queryParameters['filter'] = opt_filter;
  }

  /** @type {!Object} */
  var httpRequestParams = {
    method: 'GET',
    url: path,
    json: true,
            params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Find a model instance by {{id}} from the data source.
 * 
 * @param {!string} id Model id
 * @param {!string=} opt_filter Filter defining fields and include
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.AwCollpurDealPurchases>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesFindById = function(id, opt_filter, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases/{id}'
      .replace('{' + 'id' + '}', String(id));

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  // verify required parameter 'id' is set
  if (!id) {
    throw new Error('Missing required parameter id when calling awCollpurDealPurchasesFindById');
  }
  if (opt_filter !== undefined) {
    queryParameters['filter'] = opt_filter;
  }

  /** @type {!Object} */
  var httpRequestParams = {
    method: 'GET',
    url: path,
    json: true,
            params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Find first instance of the model matched by filter from the data source.
 * 
 * @param {!string=} opt_filter Filter defining fields, where, include, order, offset, and limit
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.AwCollpurDealPurchases>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesFindOne = function(opt_filter, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases/findOne';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  if (opt_filter !== undefined) {
    queryParameters['filter'] = opt_filter;
  }

  /** @type {!Object} */
  var httpRequestParams = {
    method: 'GET',
    url: path,
    json: true,
            params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Creates a new instance in awCollpurCoupon of this model.
 * 
 * @param {!string} id AwCollpurDealPurchases id
 * @param {!AwCollpurCoupon=} opt_data 
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.AwCollpurCoupon>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon = function(id, opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases/{id}/awCollpurCoupon'
      .replace('{' + 'id' + '}', String(id));

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  // verify required parameter 'id' is set
  if (!id) {
    throw new Error('Missing required parameter id when calling awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon');
  }
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'POST',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Deletes awCollpurCoupon of this model.
 * 
 * @param {!string} id AwCollpurDealPurchases id
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon = function(id, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases/{id}/awCollpurCoupon'
      .replace('{' + 'id' + '}', String(id));

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  // verify required parameter 'id' is set
  if (!id) {
    throw new Error('Missing required parameter id when calling awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon');
  }
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'DELETE',
    url: path,
    json: true,
            params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Fetches hasOne relation awCollpurCoupon.
 * 
 * @param {!string} id AwCollpurDealPurchases id
 * @param {!boolean=} opt_refresh 
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.AwCollpurCoupon>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesPrototypeGetAwCollpurCoupon = function(id, opt_refresh, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases/{id}/awCollpurCoupon'
      .replace('{' + 'id' + '}', String(id));

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  // verify required parameter 'id' is set
  if (!id) {
    throw new Error('Missing required parameter id when calling awCollpurDealPurchasesPrototypeGetAwCollpurCoupon');
  }
  if (opt_refresh !== undefined) {
    queryParameters['refresh'] = opt_refresh;
  }

  /** @type {!Object} */
  var httpRequestParams = {
    method: 'GET',
    url: path,
    json: true,
            params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Patch attributes for a model instance and persist it into the data source.
 * 
 * @param {!string} id AwCollpurDealPurchases id
 * @param {!AwCollpurDealPurchases=} opt_data An object of model property name/value pairs
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.AwCollpurDealPurchases>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid = function(id, opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases/{id}'
      .replace('{' + 'id' + '}', String(id));

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  // verify required parameter 'id' is set
  if (!id) {
    throw new Error('Missing required parameter id when calling awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid');
  }
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'PATCH',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Patch attributes for a model instance and persist it into the data source.
 * 
 * @param {!string} id AwCollpurDealPurchases id
 * @param {!AwCollpurDealPurchases=} opt_data An object of model property name/value pairs
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.AwCollpurDealPurchases>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid = function(id, opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases/{id}'
      .replace('{' + 'id' + '}', String(id));

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  // verify required parameter 'id' is set
  if (!id) {
    throw new Error('Missing required parameter id when calling awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid');
  }
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'PUT',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Update awCollpurCoupon of this model.
 * 
 * @param {!string} id AwCollpurDealPurchases id
 * @param {!AwCollpurCoupon=} opt_data 
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.AwCollpurCoupon>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon = function(id, opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases/{id}/awCollpurCoupon'
      .replace('{' + 'id' + '}', String(id));

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  // verify required parameter 'id' is set
  if (!id) {
    throw new Error('Missing required parameter id when calling awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon');
  }
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'PUT',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Replace attributes for a model instance and persist it into the data source.
 * 
 * @param {!string} id Model id
 * @param {!AwCollpurDealPurchases=} opt_data Model instance data
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.AwCollpurDealPurchases>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesReplaceById = function(id, opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases/{id}/replace'
      .replace('{' + 'id' + '}', String(id));

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  // verify required parameter 'id' is set
  if (!id) {
    throw new Error('Missing required parameter id when calling awCollpurDealPurchasesReplaceById');
  }
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'POST',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Replace an existing model instance or insert a new one into the data source.
 * 
 * @param {!AwCollpurDealPurchases=} opt_data Model instance data
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.AwCollpurDealPurchases>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesReplaceOrCreate = function(opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases/replaceOrCreate';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'POST',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Update instances of the model matched by {{where}} from the data source.
 * 
 * @param {!string=} opt_where Criteria to match model instances
 * @param {!AwCollpurDealPurchases=} opt_data An object of model property name/value pairs
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.inline_response_200_2>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesUpdateAll = function(opt_where, opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases/update';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  if (opt_where !== undefined) {
    queryParameters['where'] = opt_where;
  }

  /** @type {!Object} */
  var httpRequestParams = {
    method: 'POST',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Patch an existing model instance or insert a new one into the data source.
 * 
 * @param {!AwCollpurDealPurchases=} opt_data Model instance data
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.AwCollpurDealPurchases>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases = function(opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'PATCH',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Patch an existing model instance or insert a new one into the data source.
 * 
 * @param {!AwCollpurDealPurchases=} opt_data Model instance data
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.AwCollpurDealPurchases>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases = function(opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'PUT',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Update an existing model instance or insert a new one into the data source based on the where criteria.
 * 
 * @param {!string=} opt_where Criteria to match model instances
 * @param {!AwCollpurDealPurchases=} opt_data An object of model property name/value pairs
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.AwCollpurDealPurchases>}
 */
API.Client.AwCollpurDealPurchasesApi.prototype.awCollpurDealPurchasesUpsertWithWhere = function(opt_where, opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/AwCollpurDealPurchases/upsertWithWhere';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  if (opt_where !== undefined) {
    queryParameters['where'] = opt_where;
  }

  /** @type {!Object} */
  var httpRequestParams = {
    method: 'POST',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}
