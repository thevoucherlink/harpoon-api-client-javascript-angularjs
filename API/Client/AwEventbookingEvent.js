goog.provide('API.Client.AwEventbookingEvent');

/**
 * @record
 */
API.Client.AwEventbookingEvent = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingEvent.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingEvent.prototype.productId;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingEvent.prototype.isEnabled;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.AwEventbookingEvent.prototype.eventStartDate;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.AwEventbookingEvent.prototype.eventEndDate;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingEvent.prototype.dayCountBeforeSendReminderLetter;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingEvent.prototype.isReminderSend;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingEvent.prototype.isTermsEnabled;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingEvent.prototype.generatePdfTickets;

/**
 * @type {!string}
 * @export
 */
API.Client.AwEventbookingEvent.prototype.redeemRoles;

/**
 * @type {!string}
 * @export
 */
API.Client.AwEventbookingEvent.prototype.location;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.AwEventbookingEvent.prototype.tickets;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.AwEventbookingEvent.prototype.attributes;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.AwEventbookingEvent.prototype.purchasedTickets;

