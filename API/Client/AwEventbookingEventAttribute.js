goog.provide('API.Client.AwEventbookingEventAttribute');

/**
 * @record
 */
API.Client.AwEventbookingEventAttribute = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingEventAttribute.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingEventAttribute.prototype.eventId;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingEventAttribute.prototype.storeId;

/**
 * @type {!string}
 * @export
 */
API.Client.AwEventbookingEventAttribute.prototype.attributeCode;

/**
 * @type {!string}
 * @export
 */
API.Client.AwEventbookingEventAttribute.prototype.value;

