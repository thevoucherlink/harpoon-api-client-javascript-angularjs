goog.provide('API.Client.AwEventbookingEventTicket');

/**
 * @record
 */
API.Client.AwEventbookingEventTicket = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingEventTicket.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingEventTicket.prototype.eventId;

/**
 * @type {!string}
 * @export
 */
API.Client.AwEventbookingEventTicket.prototype.price;

/**
 * @type {!string}
 * @export
 */
API.Client.AwEventbookingEventTicket.prototype.priceType;

/**
 * @type {!string}
 * @export
 */
API.Client.AwEventbookingEventTicket.prototype.sku;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingEventTicket.prototype.qty;

/**
 * @type {!string}
 * @export
 */
API.Client.AwEventbookingEventTicket.prototype.codeprefix;

/**
 * @type {!string}
 * @export
 */
API.Client.AwEventbookingEventTicket.prototype.fee;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingEventTicket.prototype.isPassOnFee;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingEventTicket.prototype.chance;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.AwEventbookingEventTicket.prototype.attributes;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.AwEventbookingEventTicket.prototype.tickets;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.AwEventbookingEventTicket.prototype.awEventbookingTicket;

