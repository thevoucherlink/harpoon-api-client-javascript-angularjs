goog.provide('API.Client.AwEventbookingEventTicketAttribute');

/**
 * @record
 */
API.Client.AwEventbookingEventTicketAttribute = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingEventTicketAttribute.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingEventTicketAttribute.prototype.ticketId;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingEventTicketAttribute.prototype.storeId;

/**
 * @type {!string}
 * @export
 */
API.Client.AwEventbookingEventTicketAttribute.prototype.attributeCode;

/**
 * @type {!string}
 * @export
 */
API.Client.AwEventbookingEventTicketAttribute.prototype.value;

