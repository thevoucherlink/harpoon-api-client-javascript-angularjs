goog.provide('API.Client.AwEventbookingOrderHistory');

/**
 * @record
 */
API.Client.AwEventbookingOrderHistory = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingOrderHistory.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingOrderHistory.prototype.eventTicketId;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingOrderHistory.prototype.orderItemId;

