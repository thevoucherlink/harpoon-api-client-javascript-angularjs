goog.provide('API.Client.AwEventbookingTicket');

/**
 * @record
 */
API.Client.AwEventbookingTicket = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingTicket.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingTicket.prototype.eventTicketId;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingTicket.prototype.orderItemId;

/**
 * @type {!string}
 * @export
 */
API.Client.AwEventbookingTicket.prototype.code;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingTicket.prototype.redeemed;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingTicket.prototype.paymentStatus;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.AwEventbookingTicket.prototype.createdAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.AwEventbookingTicket.prototype.redeemedAt;

/**
 * @type {!string}
 * @export
 */
API.Client.AwEventbookingTicket.prototype.redeemedByTerminal;

/**
 * @type {!string}
 * @export
 */
API.Client.AwEventbookingTicket.prototype.fee;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingTicket.prototype.isPassOnFee;

/**
 * @type {!number}
 * @export
 */
API.Client.AwEventbookingTicket.prototype.competitionwinnerId;

