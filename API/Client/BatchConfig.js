goog.provide('API.Client.BatchConfig');

/**
 * @record
 */
API.Client.BatchConfig = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.BatchConfig.prototype.apiKey;

/**
 * @type {!string}
 * @export
 */
API.Client.BatchConfig.prototype.iOSApiKey;

/**
 * @type {!string}
 * @export
 */
API.Client.BatchConfig.prototype.androidApiKey;

/**
 * @type {!number}
 * @export
 */
API.Client.BatchConfig.prototype.id;

