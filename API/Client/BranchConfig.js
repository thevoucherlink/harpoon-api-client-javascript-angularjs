goog.provide('API.Client.BranchConfig');

/**
 * @record
 */
API.Client.BranchConfig = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.BranchConfig.prototype.branchLiveKey;

/**
 * @type {!string}
 * @export
 */
API.Client.BranchConfig.prototype.branchTestKey;

/**
 * @type {!string}
 * @export
 */
API.Client.BranchConfig.prototype.branchLiveUrl;

/**
 * @type {!string}
 * @export
 */
API.Client.BranchConfig.prototype.branchTestUrl;

/**
 * @type {!number}
 * @export
 */
API.Client.BranchConfig.prototype.id;

