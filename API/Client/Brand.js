goog.provide('API.Client.Brand');

/**
 * @record
 */
API.Client.Brand = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.Brand.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.Brand.prototype.logo;

/**
 * @type {!string}
 * @export
 */
API.Client.Brand.prototype.cover;

/**
 * @type {!string}
 * @export
 */
API.Client.Brand.prototype.description;

/**
 * @type {!string}
 * @export
 */
API.Client.Brand.prototype.email;

/**
 * @type {!string}
 * @export
 */
API.Client.Brand.prototype.phone;

/**
 * @type {!string}
 * @export
 */
API.Client.Brand.prototype.address;

/**
 * @type {!string}
 * @export
 */
API.Client.Brand.prototype.managerName;

/**
 * @type {!number}
 * @export
 */
API.Client.Brand.prototype.followerCount;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Brand.prototype.isFollowed;

/**
 * @type {!Array<!API.Client.Category>}
 * @export
 */
API.Client.Brand.prototype.categories;

/**
 * @type {!string}
 * @export
 */
API.Client.Brand.prototype.website;

/**
 * @type {!number}
 * @export
 */
API.Client.Brand.prototype.id;

