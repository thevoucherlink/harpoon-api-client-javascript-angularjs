goog.provide('API.Client.BrandFeed');

/**
 * @record
 */
API.Client.BrandFeed = function() {}

/**
 * @type {!API.Client.Brand}
 * @export
 */
API.Client.BrandFeed.prototype.brand;

/**
 * @type {!string}
 * @export
 */
API.Client.BrandFeed.prototype.message;

/**
 * @type {!string}
 * @export
 */
API.Client.BrandFeed.prototype.cover;

/**
 * @type {!API.Client.AnonymousModel_8}
 * @export
 */
API.Client.BrandFeed.prototype.related;

/**
 * @type {!string}
 * @export
 */
API.Client.BrandFeed.prototype.link;

/**
 * @type {!string}
 * @export
 */
API.Client.BrandFeed.prototype.actionCode;

/**
 * @type {!string}
 * @export
 */
API.Client.BrandFeed.prototype.privacyCode;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.BrandFeed.prototype.postedAt;

/**
 * @type {!number}
 * @export
 */
API.Client.BrandFeed.prototype.id;

