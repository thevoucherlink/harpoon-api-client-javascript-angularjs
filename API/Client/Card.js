goog.provide('API.Client.Card');

/**
 * @record
 */
API.Client.Card = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.Card.prototype.id;

/**
 * @type {!string}
 * @export
 */
API.Client.Card.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.Card.prototype.type;

/**
 * @type {!string}
 * @export
 */
API.Client.Card.prototype.funding;

/**
 * @type {!string}
 * @export
 */
API.Client.Card.prototype.lastDigits;

/**
 * @type {!string}
 * @export
 */
API.Client.Card.prototype.exMonth;

/**
 * @type {!string}
 * @export
 */
API.Client.Card.prototype.exYear;

/**
 * @type {!string}
 * @export
 */
API.Client.Card.prototype.cardholderName;

