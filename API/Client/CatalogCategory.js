goog.provide('API.Client.CatalogCategory');

/**
 * @record
 */
API.Client.CatalogCategory = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogCategory.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogCategory.prototype.parentId;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CatalogCategory.prototype.createdAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CatalogCategory.prototype.updatedAt;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.path;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogCategory.prototype.position;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogCategory.prototype.level;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogCategory.prototype.childrenCount;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogCategory.prototype.storeId;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.allChildren;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.availableSortBy;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.children;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogCategory.prototype.customApplyToProducts;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.customDesign;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CatalogCategory.prototype.customDesignFrom;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CatalogCategory.prototype.customDesignTo;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.customLayoutUpdate;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogCategory.prototype.customUseParentSettings;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.defaultSortBy;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.description;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.displayMode;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.filterPriceRange;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.image;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogCategory.prototype.includeInMenu;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogCategory.prototype.isActive;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogCategory.prototype.isAnchor;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogCategory.prototype.landingPage;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.metaDescription;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.metaKeywords;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.metaTitle;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.pageLayout;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.pathInStore;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.thumbnail;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.ummCatLabel;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.ummCatTarget;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.ummDdBlocks;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogCategory.prototype.ummDdColumns;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.ummDdProportions;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.ummDdType;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.ummDdWidth;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.urlKey;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogCategory.prototype.urlPath;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.CatalogCategory.prototype.catalogProducts;

