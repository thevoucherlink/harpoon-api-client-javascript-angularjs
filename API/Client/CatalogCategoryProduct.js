goog.provide('API.Client.CatalogCategoryProduct');

/**
 * @record
 */
API.Client.CatalogCategoryProduct = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogCategoryProduct.prototype.categoryId;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogCategoryProduct.prototype.productId;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogCategoryProduct.prototype.position;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.CatalogCategoryProduct.prototype.catalogProduct;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.CatalogCategoryProduct.prototype.catalogCategory;

