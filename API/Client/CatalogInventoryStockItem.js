goog.provide('API.Client.CatalogInventoryStockItem');

/**
 * @record
 */
API.Client.CatalogInventoryStockItem = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.backorders;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.enableQtyIncrements;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.isDecimalDivided;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.isQtyDecimal;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.isInStock;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.id;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.lowStockDate;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.manageStock;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.minQty;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.maxSaleQty;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.minSaleQty;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.notifyStockQty;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.productId;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.qty;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.stockId;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.useConfigBackorders;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.qtyIncrements;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.stockStatusChangedAuto;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.useConfigEnableQtyInc;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.useConfigMaxSaleQty;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.useConfigManageStock;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.useConfigMinQty;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.useConfigNotifyStockQty;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.useConfigMinSaleQty;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogInventoryStockItem.prototype.useConfigQtyIncrements;

