goog.provide('API.Client.CatalogProduct');

/**
 * @record
 */
API.Client.CatalogProduct = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.attributeSetId;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.typeId;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.actionText;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.agreementId;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.alias;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.altLink;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.campaignId;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CatalogProduct.prototype.campaignLiveFrom;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CatalogProduct.prototype.campaignLiveTo;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.campaignShared;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.campaignStatus;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.campaignType;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.checkoutLink;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.collectionNotes;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.cost;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.couponCodeText;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.couponCodeType;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.couponType;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CatalogProduct.prototype.createdAt;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.creatorId;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.description;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.externalId;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.facebookAttendingCount;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.facebookCategory;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.facebookId;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.facebookMaybeCount;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.facebookNode;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.facebookNoreplyCount;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.facebookPlace;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CatalogProduct.prototype.facebookUpdatedTime;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.giftMessageAvailable;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.hasOptions;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.image;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.imageLabel;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.isRecurring;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.linksExist;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.linksPurchasedSeparately;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.linksTitle;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.locationLink;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.msrp;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.msrpDisplayActualPriceType;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.msrpEnabled;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.name;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CatalogProduct.prototype.newsFromDate;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CatalogProduct.prototype.newsToDate;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.notificationBadge;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.notificationInvisible;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.notificationSilent;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.partnerId;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.passedValidation;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.price;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.priceText;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.priceType;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.priceView;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.pushwooshIds;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.pushwooshTokens;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.recurringProfile;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.redemptionType;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.requiredOptions;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.reviewHtml;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.shipmentType;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.shortDescription;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.sku;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.skuType;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.smallImage;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.smallImageLabel;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CatalogProduct.prototype.specialFromDate;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.specialPrice;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CatalogProduct.prototype.specialToDate;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.taxClassId;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.thumbnail;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.thumbnailLabel;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.udropshipCalculateRates;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.udropshipVendor;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.udropshipVendorValue;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.udtiershipRates;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.udtiershipUseCustom;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.unlockTime;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CatalogProduct.prototype.updatedAt;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.urlKey;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.urlPath;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.validationReport;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.venueList;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.visibility;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CatalogProduct.prototype.visibleFrom;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CatalogProduct.prototype.visibleTo;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.weight;

/**
 * @type {!number}
 * @export
 */
API.Client.CatalogProduct.prototype.weightType;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.termsConditions;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.competitionQuestion;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.competitionAnswer;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.competitionWinnerType;

/**
 * @type {!boolean}
 * @export
 */
API.Client.CatalogProduct.prototype.competitionMultijoinStatus;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CatalogProduct.prototype.competitionMultijoinReset;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.competitionMultijoinCooldown;

/**
 * @type {!boolean}
 * @export
 */
API.Client.CatalogProduct.prototype.competitionWinnerEmail;

/**
 * @type {!boolean}
 * @export
 */
API.Client.CatalogProduct.prototype.competitionQuestionValidate;

/**
 * @type {!string}
 * @export
 */
API.Client.CatalogProduct.prototype.connectFacebookId;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.CatalogProduct.prototype.udropshipVendors;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.CatalogProduct.prototype.productEventCompetition;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.CatalogProduct.prototype.inventory;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.CatalogProduct.prototype.catalogCategories;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.CatalogProduct.prototype.checkoutAgreement;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.CatalogProduct.prototype.awCollpurDeal;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.CatalogProduct.prototype.creator;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.CatalogProduct.prototype.udropshipVendorProducts;

