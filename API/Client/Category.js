goog.provide('API.Client.Category');

/**
 * @record
 */
API.Client.Category = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.Category.prototype.name;

/**
 * @type {!number}
 * @export
 */
API.Client.Category.prototype.productCount;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Category.prototype.hasChildren;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.Category.prototype.parent;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Category.prototype.isPrimary;

/**
 * @type {!Array<!API.Client.Category>}
 * @export
 */
API.Client.Category.prototype.children;

/**
 * @type {!number}
 * @export
 */
API.Client.Category.prototype.id;

