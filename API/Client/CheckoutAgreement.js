goog.provide('API.Client.CheckoutAgreement');

/**
 * @record
 */
API.Client.CheckoutAgreement = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.CheckoutAgreement.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.CheckoutAgreement.prototype.contentHeight;

/**
 * @type {!string}
 * @export
 */
API.Client.CheckoutAgreement.prototype.checkboxText;

/**
 * @type {!number}
 * @export
 */
API.Client.CheckoutAgreement.prototype.isActive;

/**
 * @type {!number}
 * @export
 */
API.Client.CheckoutAgreement.prototype.isHtml;

/**
 * @type {!number}
 * @export
 */
API.Client.CheckoutAgreement.prototype.id;

/**
 * @type {!string}
 * @export
 */
API.Client.CheckoutAgreement.prototype.content;

