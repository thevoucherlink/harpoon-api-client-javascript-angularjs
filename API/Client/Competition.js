goog.provide('API.Client.Competition');

/**
 * @record
 */
API.Client.Competition = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.Competition.prototype.basePrice;

/**
 * @type {!Array<!API.Client.CompetitionAttendee>}
 * @export
 */
API.Client.Competition.prototype.attendees;

/**
 * @type {!number}
 * @export
 */
API.Client.Competition.prototype.attendeeCount;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Competition.prototype.hasJoined;

/**
 * @type {!number}
 * @export
 */
API.Client.Competition.prototype.chanceCount;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Competition.prototype.isWinner;

/**
 * @type {!API.Client.FacebookEvent}
 * @export
 */
API.Client.Competition.prototype.facebook;

/**
 * @type {!Array<!API.Client.CompetitionChance>}
 * @export
 */
API.Client.Competition.prototype.chances;

/**
 * @type {!string}
 * @export
 */
API.Client.Competition.prototype.earnMoreChancesURL;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.Competition.prototype.type;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Competition.prototype.earnMoreChances;

/**
 * @type {!string}
 * @export
 */
API.Client.Competition.prototype.competitionQuestion;

/**
 * @type {!string}
 * @export
 */
API.Client.Competition.prototype.competitionAnswer;

/**
 * @type {!string}
 * @export
 */
API.Client.Competition.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.Competition.prototype.description;

/**
 * @type {!string}
 * @export
 */
API.Client.Competition.prototype.cover;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.Competition.prototype.campaignType;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.Competition.prototype.category;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.Competition.prototype.topic;

/**
 * @type {!string}
 * @export
 */
API.Client.Competition.prototype.alias;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.Competition.prototype.from;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.Competition.prototype.to;

/**
 * @type {!string}
 * @export
 */
API.Client.Competition.prototype.baseCurrency;

/**
 * @type {!string}
 * @export
 */
API.Client.Competition.prototype.priceText;

/**
 * @type {!string}
 * @export
 */
API.Client.Competition.prototype.bannerText;

/**
 * @type {!string}
 * @export
 */
API.Client.Competition.prototype.checkoutLink;

/**
 * @type {!API.Client.Venue}
 * @export
 */
API.Client.Competition.prototype.nearestVenue;

/**
 * @type {!string}
 * @export
 */
API.Client.Competition.prototype.actionText;

/**
 * @type {!string}
 * @export
 */
API.Client.Competition.prototype.status;

/**
 * @type {!string}
 * @export
 */
API.Client.Competition.prototype.collectionNotes;

/**
 * @type {!string}
 * @export
 */
API.Client.Competition.prototype.termsConditions;

/**
 * @type {!string}
 * @export
 */
API.Client.Competition.prototype.locationLink;

/**
 * @type {!string}
 * @export
 */
API.Client.Competition.prototype.altLink;

/**
 * @type {!string}
 * @export
 */
API.Client.Competition.prototype.redemptionType;

/**
 * @type {!API.Client.Brand}
 * @export
 */
API.Client.Competition.prototype.brand;

/**
 * @type {!API.Client.OfferClosestPurchase}
 * @export
 */
API.Client.Competition.prototype.closestPurchase;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Competition.prototype.isFeatured;

/**
 * @type {!number}
 * @export
 */
API.Client.Competition.prototype.qtyPerOrder;

/**
 * @type {!string}
 * @export
 */
API.Client.Competition.prototype.shareLink;

/**
 * @type {!number}
 * @export
 */
API.Client.Competition.prototype.id;

