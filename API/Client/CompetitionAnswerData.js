goog.provide('API.Client.CompetitionAnswerData');

/**
 * @record
 */
API.Client.CompetitionAnswerData = function() {}

/**
 * Competition Answer
 * @type {!string}
 * @export
 */
API.Client.CompetitionAnswerData.prototype.competitionAnswer;

