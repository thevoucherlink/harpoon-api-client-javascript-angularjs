goog.provide('API.Client.CompetitionAttendee');

/**
 * @record
 */
API.Client.CompetitionAttendee = function() {}

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CompetitionAttendee.prototype.joinedAt;

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionAttendee.prototype.email;

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionAttendee.prototype.firstName;

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionAttendee.prototype.lastName;

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionAttendee.prototype.dob;

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionAttendee.prototype.gender;

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionAttendee.prototype.password;

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionAttendee.prototype.profilePicture;

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionAttendee.prototype.cover;

/**
 * @type {!API.Client.MagentoImageUpload}
 * @export
 */
API.Client.CompetitionAttendee.prototype.profilePictureUpload;

/**
 * @type {!API.Client.MagentoImageUpload}
 * @export
 */
API.Client.CompetitionAttendee.prototype.coverUpload;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.CompetitionAttendee.prototype.metadata;

/**
 * @type {!API.Client.CustomerConnection}
 * @export
 */
API.Client.CompetitionAttendee.prototype.connection;

/**
 * @type {!number}
 * @export
 */
API.Client.CompetitionAttendee.prototype.followingCount;

/**
 * @type {!number}
 * @export
 */
API.Client.CompetitionAttendee.prototype.followerCount;

/**
 * @type {!boolean}
 * @export
 */
API.Client.CompetitionAttendee.prototype.isFollowed;

/**
 * @type {!boolean}
 * @export
 */
API.Client.CompetitionAttendee.prototype.isFollower;

/**
 * @type {!number}
 * @export
 */
API.Client.CompetitionAttendee.prototype.notificationCount;

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionAttendee.prototype.authorizationCode;

/**
 * @type {!number}
 * @export
 */
API.Client.CompetitionAttendee.prototype.id;

