goog.provide('API.Client.CompetitionChance');

/**
 * @record
 */
API.Client.CompetitionChance = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionChance.prototype.name;

/**
 * @type {!number}
 * @export
 */
API.Client.CompetitionChance.prototype.price;

/**
 * @type {!number}
 * @export
 */
API.Client.CompetitionChance.prototype.chanceCount;

/**
 * @type {!number}
 * @export
 */
API.Client.CompetitionChance.prototype.qtyBought;

/**
 * @type {!number}
 * @export
 */
API.Client.CompetitionChance.prototype.qtyTotal;

/**
 * @type {!number}
 * @export
 */
API.Client.CompetitionChance.prototype.qtyLeft;

/**
 * @type {!number}
 * @export
 */
API.Client.CompetitionChance.prototype.id;

