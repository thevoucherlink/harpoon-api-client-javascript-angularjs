goog.provide('API.Client.CompetitionCheckout');

/**
 * @record
 */
API.Client.CompetitionCheckout = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.CompetitionCheckout.prototype.cartId;

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionCheckout.prototype.url;

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionCheckout.prototype.status;

/**
 * @type {!number}
 * @export
 */
API.Client.CompetitionCheckout.prototype.id;

