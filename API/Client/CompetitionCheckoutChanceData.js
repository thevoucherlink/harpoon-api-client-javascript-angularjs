goog.provide('API.Client.CompetitionCheckoutChanceData');

/**
 * @record
 */
API.Client.CompetitionCheckoutChanceData = function() {}

/**
 * Chance to checkout
 * @type {!number}
 * @export
 */
API.Client.CompetitionCheckoutChanceData.prototype.id;

/**
 * Quantity to checkout
 * @type {!number}
 * @export
 */
API.Client.CompetitionCheckoutChanceData.prototype.qty;

