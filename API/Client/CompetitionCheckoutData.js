goog.provide('API.Client.CompetitionCheckoutData');

/**
 * @record
 */
API.Client.CompetitionCheckoutData = function() {}

/**
 * Chances to checkout
 * @type {!Array<!API.Client.CompetitionCheckoutChanceData>}
 * @export
 */
API.Client.CompetitionCheckoutData.prototype.chances;

