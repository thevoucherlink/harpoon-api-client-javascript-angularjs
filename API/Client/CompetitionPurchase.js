goog.provide('API.Client.CompetitionPurchase');

/**
 * @record
 */
API.Client.CompetitionPurchase = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.CompetitionPurchase.prototype.orderId;

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionPurchase.prototype.name;

/**
 * @type {!API.Client.Competition}
 * @export
 */
API.Client.CompetitionPurchase.prototype.competition;

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionPurchase.prototype.redemptionType;

/**
 * @type {!number}
 * @export
 */
API.Client.CompetitionPurchase.prototype.unlockTime;

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionPurchase.prototype.code;

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionPurchase.prototype.qrcode;

/**
 * @type {!boolean}
 * @export
 */
API.Client.CompetitionPurchase.prototype.isAvailable;

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionPurchase.prototype.status;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CompetitionPurchase.prototype.createdAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CompetitionPurchase.prototype.expiredAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CompetitionPurchase.prototype.redeemedAt;

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionPurchase.prototype.redeemedByTerminal;

/**
 * @type {!number}
 * @export
 */
API.Client.CompetitionPurchase.prototype.basePrice;

/**
 * @type {!number}
 * @export
 */
API.Client.CompetitionPurchase.prototype.ticketPrice;

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionPurchase.prototype.currency;

/**
 * @type {!API.Client.Customer}
 * @export
 */
API.Client.CompetitionPurchase.prototype.attendee;

/**
 * @type {!number}
 * @export
 */
API.Client.CompetitionPurchase.prototype.chanceCount;

/**
 * @type {!API.Client.CompetitionWinner}
 * @export
 */
API.Client.CompetitionPurchase.prototype.winner;

/**
 * @type {!number}
 * @export
 */
API.Client.CompetitionPurchase.prototype.id;

