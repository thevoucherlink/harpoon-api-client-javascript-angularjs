goog.provide('API.Client.CompetitionRedeemData');

/**
 * @record
 */
API.Client.CompetitionRedeemData = function() {}

/**
 * Competition purchase id
 * @type {!number}
 * @export
 */
API.Client.CompetitionRedeemData.prototype.chanceId;

