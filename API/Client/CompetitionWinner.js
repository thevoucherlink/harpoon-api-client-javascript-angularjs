goog.provide('API.Client.CompetitionWinner');

/**
 * @record
 */
API.Client.CompetitionWinner = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionWinner.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.CompetitionWinner.prototype.position;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CompetitionWinner.prototype.chosenAt;

/**
 * @type {!number}
 * @export
 */
API.Client.CompetitionWinner.prototype.id;

