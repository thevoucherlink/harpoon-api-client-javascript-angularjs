goog.provide('API.Client.Contact');

/**
 * @record
 */
API.Client.Contact = function() {}

/**
 * Email address like hello@harpoonconnect.com
 * @type {!string}
 * @export
 */
API.Client.Contact.prototype.email;

/**
 * URL like http://harpoonconnect.com or https://www.harpoonconnect.com
 * @type {!string}
 * @export
 */
API.Client.Contact.prototype.website;

/**
 * Telephone number like 08123456789
 * @type {!string}
 * @export
 */
API.Client.Contact.prototype.phone;

/**
 * @type {!API.Client.Address}
 * @export
 */
API.Client.Contact.prototype.address;

/**
 * URL starting with https://www.twitter.com/ followed by your username or id
 * @type {!string}
 * @export
 */
API.Client.Contact.prototype.twitter;

/**
 * URL starting with https://www.facebook.com/ followed by your username or id
 * @type {!string}
 * @export
 */
API.Client.Contact.prototype.facebook;

/**
 * Telephone number including country code like: +35381234567890
 * @type {!string}
 * @export
 */
API.Client.Contact.prototype.whatsapp;

/**
 * SnapChat username
 * @type {!string}
 * @export
 */
API.Client.Contact.prototype.snapchat;

/**
 * URL starting with https://plus.google.com/+ followed by your username or id
 * @type {!string}
 * @export
 */
API.Client.Contact.prototype.googlePlus;

/**
 * LinkedIn profile URL
 * @type {!string}
 * @export
 */
API.Client.Contact.prototype.linkedIn;

/**
 * A string starting with # and followed by alphanumeric characters (both lower and upper case)
 * @type {!string}
 * @export
 */
API.Client.Contact.prototype.hashtag;

/**
 * @type {!string}
 * @export
 */
API.Client.Contact.prototype.text;

/**
 * @type {!number}
 * @export
 */
API.Client.Contact.prototype.id;

