goog.provide('API.Client.CoreWebsite');

/**
 * @record
 */
API.Client.CoreWebsite = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.CoreWebsite.prototype.id;

/**
 * @type {!string}
 * @export
 */
API.Client.CoreWebsite.prototype.code;

/**
 * @type {!string}
 * @export
 */
API.Client.CoreWebsite.prototype.name;

/**
 * @type {!number}
 * @export
 */
API.Client.CoreWebsite.prototype.sortOrder;

/**
 * @type {!number}
 * @export
 */
API.Client.CoreWebsite.prototype.defaultGroupId;

/**
 * @type {!number}
 * @export
 */
API.Client.CoreWebsite.prototype.isDefault;

