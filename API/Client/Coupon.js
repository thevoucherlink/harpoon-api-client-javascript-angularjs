goog.provide('API.Client.Coupon');

/**
 * @record
 */
API.Client.Coupon = function() {}

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.Coupon.prototype.type;

/**
 * @type {!number}
 * @export
 */
API.Client.Coupon.prototype.price;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Coupon.prototype.hasClaimed;

/**
 * @type {!number}
 * @export
 */
API.Client.Coupon.prototype.qtyLeft;

/**
 * @type {!number}
 * @export
 */
API.Client.Coupon.prototype.qtyTotal;

/**
 * @type {!number}
 * @export
 */
API.Client.Coupon.prototype.qtyClaimed;

/**
 * @type {!string}
 * @export
 */
API.Client.Coupon.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.Coupon.prototype.description;

/**
 * @type {!string}
 * @export
 */
API.Client.Coupon.prototype.cover;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.Coupon.prototype.campaignType;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.Coupon.prototype.category;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.Coupon.prototype.topic;

/**
 * @type {!string}
 * @export
 */
API.Client.Coupon.prototype.alias;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.Coupon.prototype.from;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.Coupon.prototype.to;

/**
 * @type {!string}
 * @export
 */
API.Client.Coupon.prototype.baseCurrency;

/**
 * @type {!string}
 * @export
 */
API.Client.Coupon.prototype.priceText;

/**
 * @type {!string}
 * @export
 */
API.Client.Coupon.prototype.bannerText;

/**
 * @type {!string}
 * @export
 */
API.Client.Coupon.prototype.checkoutLink;

/**
 * @type {!API.Client.Venue}
 * @export
 */
API.Client.Coupon.prototype.nearestVenue;

/**
 * @type {!string}
 * @export
 */
API.Client.Coupon.prototype.actionText;

/**
 * @type {!string}
 * @export
 */
API.Client.Coupon.prototype.status;

/**
 * @type {!string}
 * @export
 */
API.Client.Coupon.prototype.collectionNotes;

/**
 * @type {!string}
 * @export
 */
API.Client.Coupon.prototype.termsConditions;

/**
 * @type {!string}
 * @export
 */
API.Client.Coupon.prototype.locationLink;

/**
 * @type {!string}
 * @export
 */
API.Client.Coupon.prototype.altLink;

/**
 * @type {!string}
 * @export
 */
API.Client.Coupon.prototype.redemptionType;

/**
 * @type {!API.Client.Brand}
 * @export
 */
API.Client.Coupon.prototype.brand;

/**
 * @type {!API.Client.OfferClosestPurchase}
 * @export
 */
API.Client.Coupon.prototype.closestPurchase;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Coupon.prototype.isFeatured;

/**
 * @type {!number}
 * @export
 */
API.Client.Coupon.prototype.qtyPerOrder;

/**
 * @type {!string}
 * @export
 */
API.Client.Coupon.prototype.shareLink;

/**
 * @type {!number}
 * @export
 */
API.Client.Coupon.prototype.id;

