goog.provide('API.Client.CouponCheckout');

/**
 * @record
 */
API.Client.CouponCheckout = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.CouponCheckout.prototype.orderId;

/**
 * @type {!string}
 * @export
 */
API.Client.CouponCheckout.prototype.name;

/**
 * @type {!API.Client.CouponPurchaseCode}
 * @export
 */
API.Client.CouponCheckout.prototype.code;

/**
 * @type {!boolean}
 * @export
 */
API.Client.CouponCheckout.prototype.isAvailable;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CouponCheckout.prototype.createdAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CouponCheckout.prototype.expiredAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CouponCheckout.prototype.redeemedAt;

/**
 * @type {!string}
 * @export
 */
API.Client.CouponCheckout.prototype.redeemedByTerminal;

/**
 * @type {!string}
 * @export
 */
API.Client.CouponCheckout.prototype.redemptionType;

/**
 * @type {!string}
 * @export
 */
API.Client.CouponCheckout.prototype.status;

/**
 * @type {!number}
 * @export
 */
API.Client.CouponCheckout.prototype.unlockTime;

/**
 * @type {!number}
 * @export
 */
API.Client.CouponCheckout.prototype.id;

