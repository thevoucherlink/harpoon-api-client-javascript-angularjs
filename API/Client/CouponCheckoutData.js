goog.provide('API.Client.CouponCheckoutData');

/**
 * @record
 */
API.Client.CouponCheckoutData = function() {}

/**
 * Quantity to checkout
 * @type {!number}
 * @export
 */
API.Client.CouponCheckoutData.prototype.qty;

