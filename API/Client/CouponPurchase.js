goog.provide('API.Client.CouponPurchase');

/**
 * @record
 */
API.Client.CouponPurchase = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.CouponPurchase.prototype.orderId;

/**
 * @type {!string}
 * @export
 */
API.Client.CouponPurchase.prototype.name;

/**
 * @type {!API.Client.CouponPurchaseCode}
 * @export
 */
API.Client.CouponPurchase.prototype.code;

/**
 * @type {!boolean}
 * @export
 */
API.Client.CouponPurchase.prototype.isAvailable;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CouponPurchase.prototype.createdAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CouponPurchase.prototype.expiredAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CouponPurchase.prototype.redeemedAt;

/**
 * @type {!string}
 * @export
 */
API.Client.CouponPurchase.prototype.redeemedByTerminal;

/**
 * @type {!string}
 * @export
 */
API.Client.CouponPurchase.prototype.redemptionType;

/**
 * @type {!string}
 * @export
 */
API.Client.CouponPurchase.prototype.status;

/**
 * @type {!number}
 * @export
 */
API.Client.CouponPurchase.prototype.unlockTime;

/**
 * @type {!number}
 * @export
 */
API.Client.CouponPurchase.prototype.id;

