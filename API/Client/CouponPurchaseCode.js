goog.provide('API.Client.CouponPurchaseCode');

/**
 * @record
 */
API.Client.CouponPurchaseCode = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.CouponPurchaseCode.prototype.type;

/**
 * @type {!string}
 * @export
 */
API.Client.CouponPurchaseCode.prototype.text;

/**
 * @type {!string}
 * @export
 */
API.Client.CouponPurchaseCode.prototype.image;

/**
 * @type {!number}
 * @export
 */
API.Client.CouponPurchaseCode.prototype.id;

