goog.provide('API.Client.CouponRedeemData');

/**
 * @record
 */
API.Client.CouponRedeemData = function() {}

/**
 * Coupon purchase id
 * @type {!number}
 * @export
 */
API.Client.CouponRedeemData.prototype.purchaseId;

