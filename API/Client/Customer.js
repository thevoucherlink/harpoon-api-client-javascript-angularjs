goog.provide('API.Client.Customer');

/**
 * @record
 */
API.Client.Customer = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.Customer.prototype.email;

/**
 * @type {!string}
 * @export
 */
API.Client.Customer.prototype.firstName;

/**
 * @type {!string}
 * @export
 */
API.Client.Customer.prototype.lastName;

/**
 * @type {!string}
 * @export
 */
API.Client.Customer.prototype.dob;

/**
 * @type {!string}
 * @export
 */
API.Client.Customer.prototype.gender;

/**
 * @type {!string}
 * @export
 */
API.Client.Customer.prototype.password;

/**
 * @type {!string}
 * @export
 */
API.Client.Customer.prototype.profilePicture;

/**
 * @type {!string}
 * @export
 */
API.Client.Customer.prototype.cover;

/**
 * @type {!API.Client.MagentoImageUpload}
 * @export
 */
API.Client.Customer.prototype.profilePictureUpload;

/**
 * @type {!API.Client.MagentoImageUpload}
 * @export
 */
API.Client.Customer.prototype.coverUpload;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.Customer.prototype.metadata;

/**
 * @type {!API.Client.CustomerConnection}
 * @export
 */
API.Client.Customer.prototype.connection;

/**
 * @type {!API.Client.CustomerPrivacy}
 * @export
 */
API.Client.Customer.prototype.privacy;

/**
 * @type {!number}
 * @export
 */
API.Client.Customer.prototype.followingCount;

/**
 * @type {!number}
 * @export
 */
API.Client.Customer.prototype.followerCount;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Customer.prototype.isFollowed;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Customer.prototype.isFollower;

/**
 * @type {!number}
 * @export
 */
API.Client.Customer.prototype.notificationCount;

/**
 * @type {!API.Client.CustomerBadge}
 * @export
 */
API.Client.Customer.prototype.badge;

/**
 * @type {!string}
 * @export
 */
API.Client.Customer.prototype.authorizationCode;

/**
 * @type {!number}
 * @export
 */
API.Client.Customer.prototype.id;

/**
 * @type {!string}
 * @export
 */
API.Client.Customer.prototype.appId;

