goog.provide('API.Client.CustomerActivity');

/**
 * @record
 */
API.Client.CustomerActivity = function() {}

/**
 * @type {!API.Client.Customer}
 * @export
 */
API.Client.CustomerActivity.prototype.customer;

/**
 * @type {!string}
 * @export
 */
API.Client.CustomerActivity.prototype.message;

/**
 * @type {!string}
 * @export
 */
API.Client.CustomerActivity.prototype.cover;

/**
 * @type {!API.Client.AnonymousModel_8}
 * @export
 */
API.Client.CustomerActivity.prototype.related;

/**
 * @type {!string}
 * @export
 */
API.Client.CustomerActivity.prototype.link;

/**
 * @type {!string}
 * @export
 */
API.Client.CustomerActivity.prototype.actionCode;

/**
 * @type {!string}
 * @export
 */
API.Client.CustomerActivity.prototype.privacyCode;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CustomerActivity.prototype.postedAt;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerActivity.prototype.id;

