goog.provide('API.Client.CustomerBadge');

/**
 * @record
 */
API.Client.CustomerBadge = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerBadge.prototype.offer;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerBadge.prototype.event;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerBadge.prototype.dealAll;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerBadge.prototype.dealCoupon;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerBadge.prototype.dealSimple;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerBadge.prototype.dealGroup;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerBadge.prototype.competition;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerBadge.prototype.walletOfferAvailable;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerBadge.prototype.walletOfferNew;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerBadge.prototype.walletOfferExpiring;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerBadge.prototype.walletOfferExpired;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerBadge.prototype.notificationUnread;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerBadge.prototype.brandFeed;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerBadge.prototype.brand;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerBadge.prototype.brandActivity;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerBadge.prototype.all;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerBadge.prototype.id;

