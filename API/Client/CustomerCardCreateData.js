goog.provide('API.Client.CustomerCardCreateData');

/**
 * @record
 */
API.Client.CustomerCardCreateData = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.CustomerCardCreateData.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.CustomerCardCreateData.prototype.number;

/**
 * @type {!string}
 * @export
 */
API.Client.CustomerCardCreateData.prototype.cvc;

/**
 * @type {!string}
 * @export
 */
API.Client.CustomerCardCreateData.prototype.exMonth;

/**
 * @type {!string}
 * @export
 */
API.Client.CustomerCardCreateData.prototype.exYear;

/**
 * @type {!string}
 * @export
 */
API.Client.CustomerCardCreateData.prototype.cardholderName;

