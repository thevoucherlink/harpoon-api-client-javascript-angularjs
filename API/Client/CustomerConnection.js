goog.provide('API.Client.CustomerConnection');

/**
 * @record
 */
API.Client.CustomerConnection = function() {}

/**
 * @type {!API.Client.CustomerConnectionFacebook}
 * @export
 */
API.Client.CustomerConnection.prototype.facebook;

/**
 * @type {!API.Client.CustomerConnectionTwitter}
 * @export
 */
API.Client.CustomerConnection.prototype.twitter;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerConnection.prototype.id;

