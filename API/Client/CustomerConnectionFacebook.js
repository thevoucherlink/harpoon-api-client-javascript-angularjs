goog.provide('API.Client.CustomerConnectionFacebook');

/**
 * @record
 */
API.Client.CustomerConnectionFacebook = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.CustomerConnectionFacebook.prototype.userId;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerConnectionFacebook.prototype.id;

