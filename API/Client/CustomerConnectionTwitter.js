goog.provide('API.Client.CustomerConnectionTwitter');

/**
 * @record
 */
API.Client.CustomerConnectionTwitter = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.CustomerConnectionTwitter.prototype.userId;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerConnectionTwitter.prototype.id;

