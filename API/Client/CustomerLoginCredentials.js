goog.provide('API.Client.CustomerLoginCredentials');

/**
 * @record
 */
API.Client.CustomerLoginCredentials = function() {}

/**
 * Customer Email
 * @type {!string}
 * @export
 */
API.Client.CustomerLoginCredentials.prototype.email;

/**
 * Customer Password
 * @type {!string}
 * @export
 */
API.Client.CustomerLoginCredentials.prototype.password;

