goog.provide('API.Client.CustomerNotification');

/**
 * @record
 */
API.Client.CustomerNotification = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.CustomerNotification.prototype.message;

/**
 * @type {!string}
 * @export
 */
API.Client.CustomerNotification.prototype.cover;

/**
 * @type {!API.Client.MagentoImageUpload}
 * @export
 */
API.Client.CustomerNotification.prototype.coverUpload;

/**
 * @type {!API.Client.NotificationFrom}
 * @export
 */
API.Client.CustomerNotification.prototype.from;

/**
 * @type {!API.Client.NotificationRelated}
 * @export
 */
API.Client.CustomerNotification.prototype.related;

/**
 * @type {!string}
 * @export
 */
API.Client.CustomerNotification.prototype.link;

/**
 * @type {!string}
 * @export
 */
API.Client.CustomerNotification.prototype.actionCode;

/**
 * @type {!string}
 * @export
 */
API.Client.CustomerNotification.prototype.status;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.CustomerNotification.prototype.sentAt;

/**
 * @type {!number}
 * @export
 */
API.Client.CustomerNotification.prototype.id;

