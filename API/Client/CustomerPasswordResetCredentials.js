goog.provide('API.Client.CustomerPasswordResetCredentials');

/**
 * @record
 */
API.Client.CustomerPasswordResetCredentials = function() {}

/**
 * Customer Email
 * @type {!string}
 * @export
 */
API.Client.CustomerPasswordResetCredentials.prototype.email;

