goog.provide('API.Client.CustomerPasswordUpdateCredentials');

/**
 * @record
 */
API.Client.CustomerPasswordUpdateCredentials = function() {}

/**
 * Current Customer Passwrod
 * @type {!string}
 * @export
 */
API.Client.CustomerPasswordUpdateCredentials.prototype.currentPassword;

/**
 * New Customer Password
 * @type {!string}
 * @export
 */
API.Client.CustomerPasswordUpdateCredentials.prototype.newPassword;

