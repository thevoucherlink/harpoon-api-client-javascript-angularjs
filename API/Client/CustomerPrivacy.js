goog.provide('API.Client.CustomerPrivacy');

/**
 * @record
 */
API.Client.CustomerPrivacy = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.CustomerPrivacy.prototype.activity;

/**
 * @type {!boolean}
 * @export
 */
API.Client.CustomerPrivacy.prototype.notificationPush;

/**
 * @type {!boolean}
 * @export
 */
API.Client.CustomerPrivacy.prototype.notificationBeacon;

/**
 * @type {!boolean}
 * @export
 */
API.Client.CustomerPrivacy.prototype.notificationLocation;

