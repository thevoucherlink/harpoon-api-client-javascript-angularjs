goog.provide('API.Client.CustomerRefreshTokenCredentials');

/**
 * @record
 */
API.Client.CustomerRefreshTokenCredentials = function() {}

/**
 * Refresh Token can be found in a logged in and authorized Customer associated to an AuthToken under the property refreshToken
 * @type {!string}
 * @export
 */
API.Client.CustomerRefreshTokenCredentials.prototype.refreshToken;

