goog.provide('API.Client.CustomerTokenFromAuthorizationCodeCredentials');

/**
 * @record
 */
API.Client.CustomerTokenFromAuthorizationCodeCredentials = function() {}

/**
 * Code can be found in a logged in Customer under the key authorizationCode
 * @type {!string}
 * @export
 */
API.Client.CustomerTokenFromAuthorizationCodeCredentials.prototype.code;

