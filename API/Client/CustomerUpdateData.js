goog.provide('API.Client.CustomerUpdateData');

/**
 * @record
 */
API.Client.CustomerUpdateData = function() {}

/**
 * New first name for the Customer
 * @type {!string}
 * @export
 */
API.Client.CustomerUpdateData.prototype.firstName;

/**
 * New last name for the Customer
 * @type {!string}
 * @export
 */
API.Client.CustomerUpdateData.prototype.lastName;

/**
 * New email for the Customer
 * @type {!string}
 * @export
 */
API.Client.CustomerUpdateData.prototype.email;

/**
 * New gender for the Customer
 * @type {!string}
 * @export
 */
API.Client.CustomerUpdateData.prototype.gender;

/**
 * New date of birth for the Customer
 * @type {!string}
 * @export
 */
API.Client.CustomerUpdateData.prototype.dob;

/**
 * New metadata collection for the Customer
 * @type {!API.Client.Object}
 * @export
 */
API.Client.CustomerUpdateData.prototype.metadata;

/**
 * New privacy settings for the Customer
 * @type {!API.Client.CustomerPrivacy}
 * @export
 */
API.Client.CustomerUpdateData.prototype.privacy;

/**
 * New profile picture for the Customer
 * @type {!API.Client.MagentoImageUpload}
 * @export
 */
API.Client.CustomerUpdateData.prototype.profilePictureUpload;

/**
 * New cover photo for the customer
 * @type {!API.Client.MagentoImageUpload}
 * @export
 */
API.Client.CustomerUpdateData.prototype.coverUpload;

