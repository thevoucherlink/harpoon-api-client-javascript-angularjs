goog.provide('API.Client.Deal');

/**
 * @record
 */
API.Client.Deal = function() {}

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.Deal.prototype.type;

/**
 * @type {!number}
 * @export
 */
API.Client.Deal.prototype.price;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Deal.prototype.hasClaimed;

/**
 * @type {!number}
 * @export
 */
API.Client.Deal.prototype.qtyLeft;

/**
 * @type {!number}
 * @export
 */
API.Client.Deal.prototype.qtyTotal;

/**
 * @type {!number}
 * @export
 */
API.Client.Deal.prototype.qtyClaimed;

/**
 * @type {!string}
 * @export
 */
API.Client.Deal.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.Deal.prototype.description;

/**
 * @type {!string}
 * @export
 */
API.Client.Deal.prototype.cover;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.Deal.prototype.campaignType;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.Deal.prototype.category;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.Deal.prototype.topic;

/**
 * @type {!string}
 * @export
 */
API.Client.Deal.prototype.alias;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.Deal.prototype.from;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.Deal.prototype.to;

/**
 * @type {!string}
 * @export
 */
API.Client.Deal.prototype.baseCurrency;

/**
 * @type {!string}
 * @export
 */
API.Client.Deal.prototype.priceText;

/**
 * @type {!string}
 * @export
 */
API.Client.Deal.prototype.bannerText;

/**
 * @type {!string}
 * @export
 */
API.Client.Deal.prototype.checkoutLink;

/**
 * @type {!API.Client.Venue}
 * @export
 */
API.Client.Deal.prototype.nearestVenue;

/**
 * @type {!string}
 * @export
 */
API.Client.Deal.prototype.actionText;

/**
 * @type {!string}
 * @export
 */
API.Client.Deal.prototype.status;

/**
 * @type {!string}
 * @export
 */
API.Client.Deal.prototype.collectionNotes;

/**
 * @type {!string}
 * @export
 */
API.Client.Deal.prototype.termsConditions;

/**
 * @type {!string}
 * @export
 */
API.Client.Deal.prototype.locationLink;

/**
 * @type {!string}
 * @export
 */
API.Client.Deal.prototype.altLink;

/**
 * @type {!string}
 * @export
 */
API.Client.Deal.prototype.redemptionType;

/**
 * @type {!API.Client.Brand}
 * @export
 */
API.Client.Deal.prototype.brand;

/**
 * @type {!API.Client.OfferClosestPurchase}
 * @export
 */
API.Client.Deal.prototype.closestPurchase;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Deal.prototype.isFeatured;

/**
 * @type {!number}
 * @export
 */
API.Client.Deal.prototype.qtyPerOrder;

/**
 * @type {!string}
 * @export
 */
API.Client.Deal.prototype.shareLink;

/**
 * @type {!number}
 * @export
 */
API.Client.Deal.prototype.id;

