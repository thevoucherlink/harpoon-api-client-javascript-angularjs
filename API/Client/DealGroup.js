goog.provide('API.Client.DealGroup');

/**
 * @record
 */
API.Client.DealGroup = function() {}

/**
 * @type {!API.Client.Product}
 * @export
 */
API.Client.DealGroup.prototype.product;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.DealGroup.prototype.type;

/**
 * @type {!number}
 * @export
 */
API.Client.DealGroup.prototype.price;

/**
 * @type {!boolean}
 * @export
 */
API.Client.DealGroup.prototype.hasClaimed;

/**
 * @type {!number}
 * @export
 */
API.Client.DealGroup.prototype.qtyLeft;

/**
 * @type {!number}
 * @export
 */
API.Client.DealGroup.prototype.qtyTotal;

/**
 * @type {!number}
 * @export
 */
API.Client.DealGroup.prototype.qtyClaimed;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroup.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroup.prototype.description;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroup.prototype.cover;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.DealGroup.prototype.campaignType;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.DealGroup.prototype.category;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.DealGroup.prototype.topic;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroup.prototype.alias;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.DealGroup.prototype.from;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.DealGroup.prototype.to;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroup.prototype.baseCurrency;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroup.prototype.priceText;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroup.prototype.bannerText;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroup.prototype.checkoutLink;

/**
 * @type {!API.Client.Venue}
 * @export
 */
API.Client.DealGroup.prototype.nearestVenue;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroup.prototype.actionText;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroup.prototype.status;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroup.prototype.collectionNotes;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroup.prototype.termsConditions;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroup.prototype.locationLink;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroup.prototype.altLink;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroup.prototype.redemptionType;

/**
 * @type {!API.Client.Brand}
 * @export
 */
API.Client.DealGroup.prototype.brand;

/**
 * @type {!API.Client.OfferClosestPurchase}
 * @export
 */
API.Client.DealGroup.prototype.closestPurchase;

/**
 * @type {!boolean}
 * @export
 */
API.Client.DealGroup.prototype.isFeatured;

/**
 * @type {!number}
 * @export
 */
API.Client.DealGroup.prototype.qtyPerOrder;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroup.prototype.shareLink;

/**
 * @type {!number}
 * @export
 */
API.Client.DealGroup.prototype.id;

