goog.provide('API.Client.DealGroupCheckout');

/**
 * @record
 */
API.Client.DealGroupCheckout = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.DealGroupCheckout.prototype.cartId;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroupCheckout.prototype.url;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroupCheckout.prototype.status;

/**
 * @type {!number}
 * @export
 */
API.Client.DealGroupCheckout.prototype.id;

