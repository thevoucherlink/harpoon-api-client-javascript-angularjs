goog.provide('API.Client.DealGroupCheckoutData');

/**
 * @record
 */
API.Client.DealGroupCheckoutData = function() {}

/**
 * Quantity to checkout
 * @type {!number}
 * @export
 */
API.Client.DealGroupCheckoutData.prototype.qty;

