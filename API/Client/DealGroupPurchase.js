goog.provide('API.Client.DealGroupPurchase');

/**
 * @record
 */
API.Client.DealGroupPurchase = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.DealGroupPurchase.prototype.orderId;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroupPurchase.prototype.name;

/**
 * @type {!API.Client.CouponPurchaseCode}
 * @export
 */
API.Client.DealGroupPurchase.prototype.code;

/**
 * @type {!boolean}
 * @export
 */
API.Client.DealGroupPurchase.prototype.isAvailable;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.DealGroupPurchase.prototype.createdAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.DealGroupPurchase.prototype.expiredAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.DealGroupPurchase.prototype.redeemedAt;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroupPurchase.prototype.redeemedByTerminal;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroupPurchase.prototype.redemptionType;

/**
 * @type {!string}
 * @export
 */
API.Client.DealGroupPurchase.prototype.status;

/**
 * @type {!number}
 * @export
 */
API.Client.DealGroupPurchase.prototype.unlockTime;

/**
 * @type {!number}
 * @export
 */
API.Client.DealGroupPurchase.prototype.id;

