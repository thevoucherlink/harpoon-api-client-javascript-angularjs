goog.provide('API.Client.DealGroupRedeemData');

/**
 * @record
 */
API.Client.DealGroupRedeemData = function() {}

/**
 * Group Deal purchase id
 * @type {!number}
 * @export
 */
API.Client.DealGroupRedeemData.prototype.purchaseId;

