goog.provide('API.Client.DealPaid');

/**
 * @record
 */
API.Client.DealPaid = function() {}

/**
 * @type {!API.Client.Product}
 * @export
 */
API.Client.DealPaid.prototype.product;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.DealPaid.prototype.type;

/**
 * @type {!number}
 * @export
 */
API.Client.DealPaid.prototype.price;

/**
 * @type {!boolean}
 * @export
 */
API.Client.DealPaid.prototype.hasClaimed;

/**
 * @type {!number}
 * @export
 */
API.Client.DealPaid.prototype.qtyLeft;

/**
 * @type {!number}
 * @export
 */
API.Client.DealPaid.prototype.qtyTotal;

/**
 * @type {!number}
 * @export
 */
API.Client.DealPaid.prototype.qtyClaimed;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaid.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaid.prototype.description;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaid.prototype.cover;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.DealPaid.prototype.campaignType;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.DealPaid.prototype.category;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.DealPaid.prototype.topic;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaid.prototype.alias;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.DealPaid.prototype.from;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.DealPaid.prototype.to;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaid.prototype.baseCurrency;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaid.prototype.priceText;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaid.prototype.bannerText;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaid.prototype.checkoutLink;

/**
 * @type {!API.Client.Venue}
 * @export
 */
API.Client.DealPaid.prototype.nearestVenue;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaid.prototype.actionText;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaid.prototype.status;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaid.prototype.collectionNotes;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaid.prototype.termsConditions;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaid.prototype.locationLink;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaid.prototype.altLink;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaid.prototype.redemptionType;

/**
 * @type {!API.Client.Brand}
 * @export
 */
API.Client.DealPaid.prototype.brand;

/**
 * @type {!API.Client.OfferClosestPurchase}
 * @export
 */
API.Client.DealPaid.prototype.closestPurchase;

/**
 * @type {!boolean}
 * @export
 */
API.Client.DealPaid.prototype.isFeatured;

/**
 * @type {!number}
 * @export
 */
API.Client.DealPaid.prototype.qtyPerOrder;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaid.prototype.shareLink;

/**
 * @type {!number}
 * @export
 */
API.Client.DealPaid.prototype.id;

