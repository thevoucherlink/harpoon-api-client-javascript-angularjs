goog.provide('API.Client.DealPaidCheckout');

/**
 * @record
 */
API.Client.DealPaidCheckout = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.DealPaidCheckout.prototype.cartId;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaidCheckout.prototype.url;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaidCheckout.prototype.status;

/**
 * @type {!number}
 * @export
 */
API.Client.DealPaidCheckout.prototype.id;

