goog.provide('API.Client.DealPaidCheckoutData');

/**
 * @record
 */
API.Client.DealPaidCheckoutData = function() {}

/**
 * Quantity to checkout
 * @type {!number}
 * @export
 */
API.Client.DealPaidCheckoutData.prototype.qty;

