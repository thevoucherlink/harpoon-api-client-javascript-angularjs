goog.provide('API.Client.DealPaidPurchase');

/**
 * @record
 */
API.Client.DealPaidPurchase = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.DealPaidPurchase.prototype.orderId;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaidPurchase.prototype.name;

/**
 * @type {!API.Client.CouponPurchaseCode}
 * @export
 */
API.Client.DealPaidPurchase.prototype.code;

/**
 * @type {!boolean}
 * @export
 */
API.Client.DealPaidPurchase.prototype.isAvailable;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.DealPaidPurchase.prototype.createdAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.DealPaidPurchase.prototype.expiredAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.DealPaidPurchase.prototype.redeemedAt;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaidPurchase.prototype.redeemedByTerminal;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaidPurchase.prototype.redemptionType;

/**
 * @type {!string}
 * @export
 */
API.Client.DealPaidPurchase.prototype.status;

/**
 * @type {!number}
 * @export
 */
API.Client.DealPaidPurchase.prototype.unlockTime;

/**
 * @type {!number}
 * @export
 */
API.Client.DealPaidPurchase.prototype.id;

