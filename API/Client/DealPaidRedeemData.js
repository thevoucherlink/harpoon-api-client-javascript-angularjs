goog.provide('API.Client.DealPaidRedeemData');

/**
 * @record
 */
API.Client.DealPaidRedeemData = function() {}

/**
 * Paid Deal purchase id
 * @type {!number}
 * @export
 */
API.Client.DealPaidRedeemData.prototype.purchaseId;

