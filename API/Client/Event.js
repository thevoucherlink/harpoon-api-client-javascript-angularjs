goog.provide('API.Client.Event');

/**
 * @record
 */
API.Client.Event = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.Event.prototype.basePrice;

/**
 * @type {!Array<!API.Client.EventAttendee>}
 * @export
 */
API.Client.Event.prototype.attendees;

/**
 * @type {!number}
 * @export
 */
API.Client.Event.prototype.attendeeCount;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Event.prototype.isGoing;

/**
 * @type {!Array<!API.Client.EventTicket>}
 * @export
 */
API.Client.Event.prototype.tickets;

/**
 * @type {!string}
 * @export
 */
API.Client.Event.prototype.termsConditions;

/**
 * @type {!API.Client.FacebookEvent}
 * @export
 */
API.Client.Event.prototype.facebook;

/**
 * @type {!string}
 * @export
 */
API.Client.Event.prototype.connectFacebookId;

/**
 * @type {!string}
 * @export
 */
API.Client.Event.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.Event.prototype.description;

/**
 * @type {!string}
 * @export
 */
API.Client.Event.prototype.cover;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.Event.prototype.campaignType;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.Event.prototype.category;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.Event.prototype.topic;

/**
 * @type {!string}
 * @export
 */
API.Client.Event.prototype.alias;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.Event.prototype.from;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.Event.prototype.to;

/**
 * @type {!string}
 * @export
 */
API.Client.Event.prototype.baseCurrency;

/**
 * @type {!string}
 * @export
 */
API.Client.Event.prototype.priceText;

/**
 * @type {!string}
 * @export
 */
API.Client.Event.prototype.bannerText;

/**
 * @type {!string}
 * @export
 */
API.Client.Event.prototype.checkoutLink;

/**
 * @type {!API.Client.Venue}
 * @export
 */
API.Client.Event.prototype.nearestVenue;

/**
 * @type {!string}
 * @export
 */
API.Client.Event.prototype.actionText;

/**
 * @type {!string}
 * @export
 */
API.Client.Event.prototype.status;

/**
 * @type {!string}
 * @export
 */
API.Client.Event.prototype.collectionNotes;

/**
 * @type {!string}
 * @export
 */
API.Client.Event.prototype.locationLink;

/**
 * @type {!string}
 * @export
 */
API.Client.Event.prototype.altLink;

/**
 * @type {!string}
 * @export
 */
API.Client.Event.prototype.redemptionType;

/**
 * @type {!API.Client.Brand}
 * @export
 */
API.Client.Event.prototype.brand;

/**
 * @type {!API.Client.OfferClosestPurchase}
 * @export
 */
API.Client.Event.prototype.closestPurchase;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Event.prototype.isFeatured;

/**
 * @type {!number}
 * @export
 */
API.Client.Event.prototype.qtyPerOrder;

/**
 * @type {!string}
 * @export
 */
API.Client.Event.prototype.shareLink;

/**
 * @type {!number}
 * @export
 */
API.Client.Event.prototype.id;

