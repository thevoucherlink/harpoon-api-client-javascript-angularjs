goog.provide('API.Client.EventAttendee');

/**
 * @record
 */
API.Client.EventAttendee = function() {}

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.EventAttendee.prototype.joinedAt;

/**
 * @type {!string}
 * @export
 */
API.Client.EventAttendee.prototype.email;

/**
 * @type {!string}
 * @export
 */
API.Client.EventAttendee.prototype.firstName;

/**
 * @type {!string}
 * @export
 */
API.Client.EventAttendee.prototype.lastName;

/**
 * @type {!string}
 * @export
 */
API.Client.EventAttendee.prototype.dob;

/**
 * @type {!string}
 * @export
 */
API.Client.EventAttendee.prototype.gender;

/**
 * @type {!string}
 * @export
 */
API.Client.EventAttendee.prototype.password;

/**
 * @type {!string}
 * @export
 */
API.Client.EventAttendee.prototype.profilePicture;

/**
 * @type {!string}
 * @export
 */
API.Client.EventAttendee.prototype.cover;

/**
 * @type {!API.Client.MagentoImageUpload}
 * @export
 */
API.Client.EventAttendee.prototype.profilePictureUpload;

/**
 * @type {!API.Client.MagentoImageUpload}
 * @export
 */
API.Client.EventAttendee.prototype.coverUpload;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.EventAttendee.prototype.metadata;

/**
 * @type {!API.Client.CustomerConnection}
 * @export
 */
API.Client.EventAttendee.prototype.connection;

/**
 * @type {!number}
 * @export
 */
API.Client.EventAttendee.prototype.followingCount;

/**
 * @type {!number}
 * @export
 */
API.Client.EventAttendee.prototype.followerCount;

/**
 * @type {!boolean}
 * @export
 */
API.Client.EventAttendee.prototype.isFollowed;

/**
 * @type {!boolean}
 * @export
 */
API.Client.EventAttendee.prototype.isFollower;

/**
 * @type {!number}
 * @export
 */
API.Client.EventAttendee.prototype.notificationCount;

/**
 * @type {!string}
 * @export
 */
API.Client.EventAttendee.prototype.authorizationCode;

/**
 * @type {!number}
 * @export
 */
API.Client.EventAttendee.prototype.id;

