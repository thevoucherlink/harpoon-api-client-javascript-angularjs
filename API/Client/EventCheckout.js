goog.provide('API.Client.EventCheckout');

/**
 * @record
 */
API.Client.EventCheckout = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.EventCheckout.prototype.cartId;

/**
 * @type {!string}
 * @export
 */
API.Client.EventCheckout.prototype.url;

/**
 * @type {!string}
 * @export
 */
API.Client.EventCheckout.prototype.status;

/**
 * @type {!number}
 * @export
 */
API.Client.EventCheckout.prototype.id;

