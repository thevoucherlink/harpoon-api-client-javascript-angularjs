goog.provide('API.Client.EventCheckoutData');

/**
 * @record
 */
API.Client.EventCheckoutData = function() {}

/**
 * Tickets to checkout
 * @type {!Array<!API.Client.EventCheckoutTicketData>}
 * @export
 */
API.Client.EventCheckoutData.prototype.tickets;

