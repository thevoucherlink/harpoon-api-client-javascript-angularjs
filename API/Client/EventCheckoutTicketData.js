goog.provide('API.Client.EventCheckoutTicketData');

/**
 * @record
 */
API.Client.EventCheckoutTicketData = function() {}

/**
 * Ticket to checkout
 * @type {!number}
 * @export
 */
API.Client.EventCheckoutTicketData.prototype.id;

/**
 * Quantity to checkout
 * @type {!number}
 * @export
 */
API.Client.EventCheckoutTicketData.prototype.qty;

