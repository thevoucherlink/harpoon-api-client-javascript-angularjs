goog.provide('API.Client.EventPurchase');

/**
 * @record
 */
API.Client.EventPurchase = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.EventPurchase.prototype.orderId;

/**
 * @type {!string}
 * @export
 */
API.Client.EventPurchase.prototype.name;

/**
 * @type {!API.Client.Event}
 * @export
 */
API.Client.EventPurchase.prototype.event;

/**
 * @type {!string}
 * @export
 */
API.Client.EventPurchase.prototype.redemptionType;

/**
 * @type {!number}
 * @export
 */
API.Client.EventPurchase.prototype.unlockTime;

/**
 * @type {!string}
 * @export
 */
API.Client.EventPurchase.prototype.code;

/**
 * @type {!string}
 * @export
 */
API.Client.EventPurchase.prototype.qrcode;

/**
 * @type {!boolean}
 * @export
 */
API.Client.EventPurchase.prototype.isAvailable;

/**
 * @type {!string}
 * @export
 */
API.Client.EventPurchase.prototype.status;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.EventPurchase.prototype.createdAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.EventPurchase.prototype.expiredAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.EventPurchase.prototype.redeemedAt;

/**
 * @type {!string}
 * @export
 */
API.Client.EventPurchase.prototype.redeemedByTerminal;

/**
 * @type {!number}
 * @export
 */
API.Client.EventPurchase.prototype.basePrice;

/**
 * @type {!number}
 * @export
 */
API.Client.EventPurchase.prototype.ticketPrice;

/**
 * @type {!string}
 * @export
 */
API.Client.EventPurchase.prototype.currency;

/**
 * @type {!API.Client.Customer}
 * @export
 */
API.Client.EventPurchase.prototype.attendee;

/**
 * @type {!number}
 * @export
 */
API.Client.EventPurchase.prototype.id;

