goog.provide('API.Client.EventRedeemData');

/**
 * @record
 */
API.Client.EventRedeemData = function() {}

/**
 * Event purchase id
 * @type {!number}
 * @export
 */
API.Client.EventRedeemData.prototype.purchaseId;

