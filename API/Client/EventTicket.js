goog.provide('API.Client.EventTicket');

/**
 * @record
 */
API.Client.EventTicket = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.EventTicket.prototype.name;

/**
 * @type {!number}
 * @export
 */
API.Client.EventTicket.prototype.price;

/**
 * @type {!number}
 * @export
 */
API.Client.EventTicket.prototype.qtyBought;

/**
 * @type {!number}
 * @export
 */
API.Client.EventTicket.prototype.qtyTotal;

/**
 * @type {!number}
 * @export
 */
API.Client.EventTicket.prototype.qtyLeft;

/**
 * @type {!number}
 * @export
 */
API.Client.EventTicket.prototype.id;

