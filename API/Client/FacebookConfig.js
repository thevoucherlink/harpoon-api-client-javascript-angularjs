goog.provide('API.Client.FacebookConfig');

/**
 * @record
 */
API.Client.FacebookConfig = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.FacebookConfig.prototype.appId;

/**
 * @type {!string}
 * @export
 */
API.Client.FacebookConfig.prototype.appToken;

/**
 * @type {!string}
 * @export
 */
API.Client.FacebookConfig.prototype.scopes;

