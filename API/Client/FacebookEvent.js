goog.provide('API.Client.FacebookEvent');

/**
 * @record
 */
API.Client.FacebookEvent = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.FacebookEvent.prototype.node;

/**
 * @type {!string}
 * @export
 */
API.Client.FacebookEvent.prototype.category;

/**
 * @type {!string}
 * @export
 */
API.Client.FacebookEvent.prototype.updatedTime;

/**
 * @type {!number}
 * @export
 */
API.Client.FacebookEvent.prototype.maybeCount;

/**
 * @type {!number}
 * @export
 */
API.Client.FacebookEvent.prototype.noreplyCount;

/**
 * @type {!number}
 * @export
 */
API.Client.FacebookEvent.prototype.attendingCount;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.FacebookEvent.prototype.place;

/**
 * @type {!number}
 * @export
 */
API.Client.FacebookEvent.prototype.id;

