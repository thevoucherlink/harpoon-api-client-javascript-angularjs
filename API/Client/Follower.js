goog.provide('API.Client.Follower');

/**
 * @record
 */
API.Client.Follower = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.Follower.prototype.email;

/**
 * @type {!string}
 * @export
 */
API.Client.Follower.prototype.firstName;

/**
 * @type {!string}
 * @export
 */
API.Client.Follower.prototype.lastName;

/**
 * @type {!string}
 * @export
 */
API.Client.Follower.prototype.dob;

/**
 * @type {!string}
 * @export
 */
API.Client.Follower.prototype.gender;

/**
 * @type {!string}
 * @export
 */
API.Client.Follower.prototype.password;

/**
 * @type {!string}
 * @export
 */
API.Client.Follower.prototype.profilePicture;

/**
 * @type {!string}
 * @export
 */
API.Client.Follower.prototype.cover;

/**
 * @type {!API.Client.MagentoImageUpload}
 * @export
 */
API.Client.Follower.prototype.profilePictureUpload;

/**
 * @type {!API.Client.MagentoImageUpload}
 * @export
 */
API.Client.Follower.prototype.coverUpload;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.Follower.prototype.metadata;

/**
 * @type {!API.Client.CustomerConnection}
 * @export
 */
API.Client.Follower.prototype.connection;

/**
 * @type {!number}
 * @export
 */
API.Client.Follower.prototype.followingCount;

/**
 * @type {!number}
 * @export
 */
API.Client.Follower.prototype.followerCount;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Follower.prototype.isFollowed;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Follower.prototype.isFollower;

/**
 * @type {!number}
 * @export
 */
API.Client.Follower.prototype.notificationCount;

/**
 * @type {!string}
 * @export
 */
API.Client.Follower.prototype.authorizationCode;

/**
 * @type {!number}
 * @export
 */
API.Client.Follower.prototype.id;

