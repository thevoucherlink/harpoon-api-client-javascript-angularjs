goog.provide('API.Client.Following');

/**
 * @record
 */
API.Client.Following = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.Following.prototype.email;

/**
 * @type {!string}
 * @export
 */
API.Client.Following.prototype.firstName;

/**
 * @type {!string}
 * @export
 */
API.Client.Following.prototype.lastName;

/**
 * @type {!string}
 * @export
 */
API.Client.Following.prototype.dob;

/**
 * @type {!string}
 * @export
 */
API.Client.Following.prototype.gender;

/**
 * @type {!string}
 * @export
 */
API.Client.Following.prototype.password;

/**
 * @type {!string}
 * @export
 */
API.Client.Following.prototype.profilePicture;

/**
 * @type {!string}
 * @export
 */
API.Client.Following.prototype.cover;

/**
 * @type {!API.Client.MagentoImageUpload}
 * @export
 */
API.Client.Following.prototype.profilePictureUpload;

/**
 * @type {!API.Client.MagentoImageUpload}
 * @export
 */
API.Client.Following.prototype.coverUpload;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.Following.prototype.metadata;

/**
 * @type {!API.Client.CustomerConnection}
 * @export
 */
API.Client.Following.prototype.connection;

/**
 * @type {!number}
 * @export
 */
API.Client.Following.prototype.followingCount;

/**
 * @type {!number}
 * @export
 */
API.Client.Following.prototype.followerCount;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Following.prototype.isFollowed;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Following.prototype.isFollower;

/**
 * @type {!number}
 * @export
 */
API.Client.Following.prototype.notificationCount;

/**
 * @type {!string}
 * @export
 */
API.Client.Following.prototype.authorizationCode;

/**
 * @type {!number}
 * @export
 */
API.Client.Following.prototype.id;

