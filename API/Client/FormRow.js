goog.provide('API.Client.FormRow');

/**
 * @record
 */
API.Client.FormRow = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.FormRow.prototype.metadataKey;

/**
 * @type {!string}
 * @export
 */
API.Client.FormRow.prototype.type;

/**
 * @type {!string}
 * @export
 */
API.Client.FormRow.prototype.title;

/**
 * @type {!string}
 * @export
 */
API.Client.FormRow.prototype.defaultValue;

/**
 * @type {!string}
 * @export
 */
API.Client.FormRow.prototype.placeholder;

/**
 * @type {!boolean}
 * @export
 */
API.Client.FormRow.prototype.hidden;

/**
 * @type {!boolean}
 * @export
 */
API.Client.FormRow.prototype.required;

/**
 * @type {!string}
 * @export
 */
API.Client.FormRow.prototype.validationRegExp;

/**
 * @type {!string}
 * @export
 */
API.Client.FormRow.prototype.validationMessage;

/**
 * @type {!Array<!API.Client.FormSelectorOption>}
 * @export
 */
API.Client.FormRow.prototype.selectorOptions;

/** @enum {string} */
API.Client.FormRow.TypeEnum = { 
  text: 'text',
  name: 'name',
  url: 'url',
  email: 'email',
  password: 'password',
  number: 'number',
  phone: 'phone',
  twitter: 'twitter',
  account: 'account',
  integer: 'integer',
  decimal: 'decimal',
  textView: 'textView',
  selectorPush: 'selectorPush',
  selectorPickerView: 'selectorPickerView',
  dateInline: 'dateInline',
  datetimeInline: 'datetimeInline',
  timeInline: 'timeInline',
  countDownTimerInline: 'countDownTimerInline',
  date: 'date',
  datetime: 'datetime',
  time: 'time',
  countDownTimer: 'countDownTimer',
}
