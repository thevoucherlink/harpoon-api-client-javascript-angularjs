goog.provide('API.Client.FormSection');

/**
 * @record
 */
API.Client.FormSection = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.FormSection.prototype.title;

/**
 * @type {!Array<!API.Client.FormRow>}
 * @export
 */
API.Client.FormSection.prototype.rows;

/**
 * @type {!number}
 * @export
 */
API.Client.FormSection.prototype.id;

