goog.provide('API.Client.FormSelectorOption');

/**
 * @record
 */
API.Client.FormSelectorOption = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.FormSelectorOption.prototype.title;

/**
 * @type {!string}
 * @export
 */
API.Client.FormSelectorOption.prototype.value;

/**
 * @type {!string}
 * @export
 */
API.Client.FormSelectorOption.prototype.format;

/**
 * @type {!number}
 * @export
 */
API.Client.FormSelectorOption.prototype.id;

/** @enum {string} */
API.Client.FormSelectorOption.FormatEnum = { 
  text: 'text',
  number: 'number',
}
