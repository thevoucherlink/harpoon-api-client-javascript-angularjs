goog.provide('API.Client.GeoLocation');

/**
 * @record
 */
API.Client.GeoLocation = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.GeoLocation.prototype.latitude;

/**
 * @type {!number}
 * @export
 */
API.Client.GeoLocation.prototype.longitude;

/**
 * @type {!number}
 * @export
 */
API.Client.GeoLocation.prototype.id;

