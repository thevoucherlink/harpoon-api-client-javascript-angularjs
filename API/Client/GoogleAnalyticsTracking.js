goog.provide('API.Client.GoogleAnalyticsTracking');

/**
 * @record
 */
API.Client.GoogleAnalyticsTracking = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.GoogleAnalyticsTracking.prototype.mobileTrackingId;

/**
 * @type {!string}
 * @export
 */
API.Client.GoogleAnalyticsTracking.prototype.webTrackingId;

