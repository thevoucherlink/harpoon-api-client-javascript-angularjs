goog.provide('API.Client.HarpoonHpublicApplicationpartner');

/**
 * @record
 */
API.Client.HarpoonHpublicApplicationpartner = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.applicationId;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.brandId;

/**
 * @type {!string}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.status;

/**
 * @type {!string}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.invitedEmail;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.invitedAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.acceptedAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.createdAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.updatedAt;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.configList;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.configFeed;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.configNeedFollow;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.configAcceptEvent;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.configAcceptCoupon;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.configAcceptDealsimple;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.configAcceptDealgroup;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.configAcceptNotificationpush;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.configAcceptNotificationbeacon;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.isOwner;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.configApproveEvent;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.configApproveCoupon;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.configApproveDealsimple;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.configApproveDealgroup;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.HarpoonHpublicApplicationpartner.prototype.udropshipVendor;

