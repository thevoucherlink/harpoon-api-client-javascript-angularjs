goog.provide('API.Client.HarpoonHpublicBrandvenue');

/**
 * @record
 */
API.Client.HarpoonHpublicBrandvenue = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicBrandvenue.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicBrandvenue.prototype.brandId;

/**
 * @type {!string}
 * @export
 */
API.Client.HarpoonHpublicBrandvenue.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.HarpoonHpublicBrandvenue.prototype.address;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicBrandvenue.prototype.latitude;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicBrandvenue.prototype.longitude;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.HarpoonHpublicBrandvenue.prototype.createdAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.HarpoonHpublicBrandvenue.prototype.updatedAt;

/**
 * @type {!string}
 * @export
 */
API.Client.HarpoonHpublicBrandvenue.prototype.email;

/**
 * @type {!string}
 * @export
 */
API.Client.HarpoonHpublicBrandvenue.prototype.phone;

/**
 * @type {!string}
 * @export
 */
API.Client.HarpoonHpublicBrandvenue.prototype.country;

