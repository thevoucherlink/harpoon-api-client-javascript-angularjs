goog.provide('API.Client.HarpoonHpublicVendorconfig');

/**
 * @record
 */
API.Client.HarpoonHpublicVendorconfig = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.type;

/**
 * @type {!string}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.category;

/**
 * @type {!string}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.logo;

/**
 * @type {!string}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.cover;

/**
 * @type {!string}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.facebookPageId;

/**
 * @type {!string}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.facebookPageToken;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.facebookEventEnabled;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.facebookEventUpdatedAt;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.facebookEventCount;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.facebookFeedEnabled;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.facebookFeedUpdatedAt;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.facebookFeedCount;

/**
 * @type {!string}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.stripePublishableKey;

/**
 * @type {!string}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.stripeRefreshToken;

/**
 * @type {!string}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.stripeAccessToken;

/**
 * @type {!string}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.feesEventTicket;

/**
 * @type {!string}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.stripeUserId;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.vendorconfigId;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.createdAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.HarpoonHpublicVendorconfig.prototype.updatedAt;

