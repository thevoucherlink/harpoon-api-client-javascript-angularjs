goog.provide('API.Client.HarpoonHpublicv12VendorAppCategory');

/**
 * @record
 */
API.Client.HarpoonHpublicv12VendorAppCategory = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicv12VendorAppCategory.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicv12VendorAppCategory.prototype.appId;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicv12VendorAppCategory.prototype.vendorId;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicv12VendorAppCategory.prototype.categoryId;

/**
 * @type {!number}
 * @export
 */
API.Client.HarpoonHpublicv12VendorAppCategory.prototype.isPrimary;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.HarpoonHpublicv12VendorAppCategory.prototype.createdAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.HarpoonHpublicv12VendorAppCategory.prototype.updatedAt;

