goog.provide('API.Client.inline_response_200');

/**
 * @record
 */
API.Client.InlineResponse200 = function() {}

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.InlineResponse200.prototype.app;

