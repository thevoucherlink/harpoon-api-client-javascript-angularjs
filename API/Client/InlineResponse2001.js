goog.provide('API.Client.inline_response_200_1');

/**
 * @record
 */
API.Client.InlineResponse2001 = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.InlineResponse2001.prototype.count;

