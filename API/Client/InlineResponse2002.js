goog.provide('API.Client.inline_response_200_2');

/**
 * Information related to the outcome of the operation
 * @record
 */
API.Client.InlineResponse2002 = function() {}

/**
 * The number of instances updated
 * @type {!number}
 * @export
 */
API.Client.InlineResponse2002.prototype.count;

