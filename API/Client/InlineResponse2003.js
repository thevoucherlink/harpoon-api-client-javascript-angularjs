goog.provide('API.Client.inline_response_200_3');

/**
 * @record
 */
API.Client.InlineResponse2003 = function() {}

/**
 * @type {!boolean}
 * @export
 */
API.Client.InlineResponse2003.prototype.exists;

