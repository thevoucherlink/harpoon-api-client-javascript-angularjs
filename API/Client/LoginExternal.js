goog.provide('API.Client.LoginExternal');

/**
 * @record
 */
API.Client.LoginExternal = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.LoginExternal.prototype.apiKey;

/**
 * @type {!string}
 * @export
 */
API.Client.LoginExternal.prototype.version;

/**
 * @type {!string}
 * @export
 */
API.Client.LoginExternal.prototype.requestUrl;

/**
 * @type {!string}
 * @export
 */
API.Client.LoginExternal.prototype.authorizeUrl;

/**
 * @type {!string}
 * @export
 */
API.Client.LoginExternal.prototype.accessUrl;

