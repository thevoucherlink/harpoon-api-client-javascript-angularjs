goog.provide('API.Client.MagentoFileUpload');

/**
 * @record
 */
API.Client.MagentoFileUpload = function() {}

/**
 * File contents encoded as base64
 * @type {!string}
 * @export
 */
API.Client.MagentoFileUpload.prototype.file;

/**
 * The file's encode type e.g application/json
 * @type {!string}
 * @export
 */
API.Client.MagentoFileUpload.prototype.contentType;

/**
 * the file name including the extension
 * @type {!string}
 * @export
 */
API.Client.MagentoFileUpload.prototype.name;

