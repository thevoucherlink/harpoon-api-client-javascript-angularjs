goog.provide('API.Client.MagentoImageUpload');

/**
 * @record
 */
API.Client.MagentoImageUpload = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.MagentoImageUpload.prototype.file;

/**
 * @type {!string}
 * @export
 */
API.Client.MagentoImageUpload.prototype.contentType;

