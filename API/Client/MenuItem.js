goog.provide('API.Client.MenuItem');

/**
 * @record
 */
API.Client.MenuItem = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.MenuItem.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.MenuItem.prototype.url;

/**
 * @type {!string}
 * @export
 */
API.Client.MenuItem.prototype.image;

/**
 * @type {!boolean}
 * @export
 */
API.Client.MenuItem.prototype.visibility;

/**
 * @type {!number}
 * @export
 */
API.Client.MenuItem.prototype.sortOrder;

/**
 * @type {!boolean}
 * @export
 */
API.Client.MenuItem.prototype.pageAction;

/**
 * @type {!string}
 * @export
 */
API.Client.MenuItem.prototype.pageActionIcon;

/**
 * @type {!string}
 * @export
 */
API.Client.MenuItem.prototype.pageActionLink;

/**
 * @type {!string}
 * @export
 */
API.Client.MenuItem.prototype.styleCss;

/**
 * @type {!boolean}
 * @export
 */
API.Client.MenuItem.prototype.featureRadioDontShowOnCamera;

/**
 * @type {!number}
 * @export
 */
API.Client.MenuItem.prototype.id;

