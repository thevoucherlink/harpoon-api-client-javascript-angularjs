goog.provide('API.Client.NotificationFrom');

/**
 * @record
 */
API.Client.NotificationFrom = function() {}

/**
 * @type {!API.Client.Customer}
 * @export
 */
API.Client.NotificationFrom.prototype.customer;

/**
 * @type {!API.Client.Brand}
 * @export
 */
API.Client.NotificationFrom.prototype.brand;

/**
 * @type {!number}
 * @export
 */
API.Client.NotificationFrom.prototype.id;

