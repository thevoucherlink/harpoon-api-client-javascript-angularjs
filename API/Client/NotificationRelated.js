goog.provide('API.Client.NotificationRelated');

/**
 * @record
 */
API.Client.NotificationRelated = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.NotificationRelated.prototype.customerId;

/**
 * @type {!number}
 * @export
 */
API.Client.NotificationRelated.prototype.brandId;

/**
 * @type {!number}
 * @export
 */
API.Client.NotificationRelated.prototype.venueId;

/**
 * @type {!number}
 * @export
 */
API.Client.NotificationRelated.prototype.competitionId;

/**
 * @type {!number}
 * @export
 */
API.Client.NotificationRelated.prototype.couponId;

/**
 * @type {!number}
 * @export
 */
API.Client.NotificationRelated.prototype.eventId;

/**
 * @type {!number}
 * @export
 */
API.Client.NotificationRelated.prototype.dealPaidId;

/**
 * @type {!number}
 * @export
 */
API.Client.NotificationRelated.prototype.dealGroupId;

/**
 * @type {!number}
 * @export
 */
API.Client.NotificationRelated.prototype.id;

