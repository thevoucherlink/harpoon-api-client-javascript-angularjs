goog.provide('API.Client.Offer');

/**
 * @record
 */
API.Client.Offer = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.Offer.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.Offer.prototype.description;

/**
 * @type {!string}
 * @export
 */
API.Client.Offer.prototype.cover;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.Offer.prototype.campaignType;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.Offer.prototype.category;

/**
 * @type {!API.Client.Category}
 * @export
 */
API.Client.Offer.prototype.topic;

/**
 * @type {!string}
 * @export
 */
API.Client.Offer.prototype.alias;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.Offer.prototype.from;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.Offer.prototype.to;

/**
 * @type {!string}
 * @export
 */
API.Client.Offer.prototype.baseCurrency;

/**
 * @type {!string}
 * @export
 */
API.Client.Offer.prototype.priceText;

/**
 * @type {!string}
 * @export
 */
API.Client.Offer.prototype.bannerText;

/**
 * @type {!string}
 * @export
 */
API.Client.Offer.prototype.checkoutLink;

/**
 * @type {!API.Client.Venue}
 * @export
 */
API.Client.Offer.prototype.nearestVenue;

/**
 * @type {!string}
 * @export
 */
API.Client.Offer.prototype.actionText;

/**
 * @type {!string}
 * @export
 */
API.Client.Offer.prototype.status;

/**
 * @type {!string}
 * @export
 */
API.Client.Offer.prototype.collectionNotes;

/**
 * @type {!string}
 * @export
 */
API.Client.Offer.prototype.termsConditions;

/**
 * @type {!string}
 * @export
 */
API.Client.Offer.prototype.locationLink;

/**
 * @type {!string}
 * @export
 */
API.Client.Offer.prototype.altLink;

/**
 * @type {!string}
 * @export
 */
API.Client.Offer.prototype.redemptionType;

/**
 * @type {!API.Client.Brand}
 * @export
 */
API.Client.Offer.prototype.brand;

/**
 * @type {!API.Client.OfferClosestPurchase}
 * @export
 */
API.Client.Offer.prototype.closestPurchase;

/**
 * @type {!boolean}
 * @export
 */
API.Client.Offer.prototype.isFeatured;

/**
 * @type {!number}
 * @export
 */
API.Client.Offer.prototype.qtyPerOrder;

/**
 * @type {!string}
 * @export
 */
API.Client.Offer.prototype.shareLink;

/**
 * @type {!number}
 * @export
 */
API.Client.Offer.prototype.id;

