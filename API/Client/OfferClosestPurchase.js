goog.provide('API.Client.OfferClosestPurchase');

/**
 * @record
 */
API.Client.OfferClosestPurchase = function() {}

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.OfferClosestPurchase.prototype.expiredAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.OfferClosestPurchase.prototype.purchasedAt;

/**
 * @type {!number}
 * @export
 */
API.Client.OfferClosestPurchase.prototype.id;

