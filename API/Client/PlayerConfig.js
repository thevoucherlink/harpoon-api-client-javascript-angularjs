goog.provide('API.Client.PlayerConfig');

/**
 * @record
 */
API.Client.PlayerConfig = function() {}

/**
 * Available on the following SDKs: JS, iOS
 * @type {!boolean}
 * @export
 */
API.Client.PlayerConfig.prototype.mute;

/**
 * Available on the following SDKs: JS, iOS, Android
 * @type {!boolean}
 * @export
 */
API.Client.PlayerConfig.prototype.autostart;

/**
 * Available on the following SDKs: JS, iOS, Android
 * @type {!boolean}
 * @export
 */
API.Client.PlayerConfig.prototype.repeat;

/**
 * Available on the following SDKs: JS, iOS, Android
 * @type {!boolean}
 * @export
 */
API.Client.PlayerConfig.prototype.controls;

/**
 * Available on the following SDKs: JS (as visualplaylist)
 * @type {!boolean}
 * @export
 */
API.Client.PlayerConfig.prototype.visualPlaylist;

/**
 * Available on the following SDKs: JS (as displaytitle)
 * @type {!boolean}
 * @export
 */
API.Client.PlayerConfig.prototype.displayTitle;

/**
 * Available on the following SDKs: JS (as displaydescription)
 * @type {!boolean}
 * @export
 */
API.Client.PlayerConfig.prototype.displayDescription;

/**
 * Available on the following SDKs: JS, iOS, Android (as stretch)
 * @type {!string}
 * @export
 */
API.Client.PlayerConfig.prototype.stretching;

/**
 * Available on the following SDKs: JS
 * @type {!boolean}
 * @export
 */
API.Client.PlayerConfig.prototype.hlshtml;

/**
 * Available on the following SDKs: JS
 * @type {!string}
 * @export
 */
API.Client.PlayerConfig.prototype.primary;

/**
 * Available on the following SDKs: JS (as flashplayer)
 * @type {!string}
 * @export
 */
API.Client.PlayerConfig.prototype.flashPlayer;

/**
 * Available on the following SDKs: JS (as base)
 * @type {!string}
 * @export
 */
API.Client.PlayerConfig.prototype.baseSkinPath;

/**
 * Available on the following SDKs: JS
 * @type {!string}
 * @export
 */
API.Client.PlayerConfig.prototype.preload;

/**
 * Available on the following SDKs: JS, iOS, Android
 * @type {!API.Client.Object}
 * @export
 */
API.Client.PlayerConfig.prototype.playerAdConfig;

/**
 * Available on the following SDKs: JS, iOS, Android
 * @type {!API.Client.Object}
 * @export
 */
API.Client.PlayerConfig.prototype.playerCaptionConfig;

/**
 * Available on the following SDKs: JS (as skin.name), iOS, Android (as premiumSkin)
 * @type {!string}
 * @export
 */
API.Client.PlayerConfig.prototype.skinName;

/**
 * Available on the following SDKs: JS (as skin.url), iOS (as skinUrl), Android (as cssSkin)
 * @type {!string}
 * @export
 */
API.Client.PlayerConfig.prototype.skinCss;

/**
 * Value must be a valid HEX color. Available on the following SDKs: JS (as skin.active), iOS
 * @type {!string}
 * @export
 */
API.Client.PlayerConfig.prototype.skinActive;

/**
 * Value must be a valid HEX color. Available on the following SDKs: JS (as skin.inactive), iOS
 * @type {!string}
 * @export
 */
API.Client.PlayerConfig.prototype.skinInactive;

/**
 * Value must be a valid HEX color. Available on the following SDKs: JS (as skin.background), iOS
 * @type {!string}
 * @export
 */
API.Client.PlayerConfig.prototype.skinBackground;

/**
 * @type {!number}
 * @export
 */
API.Client.PlayerConfig.prototype.id;

