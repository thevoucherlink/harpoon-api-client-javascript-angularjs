goog.provide('API.Client.PlayerSource');

/**
 * @record
 */
API.Client.PlayerSource = function() {}

/**
 * Source file
 * @type {!string}
 * @export
 */
API.Client.PlayerSource.prototype.file;

/**
 * Source type (used only in JS SDK)
 * @type {!string}
 * @export
 */
API.Client.PlayerSource.prototype.type;

/**
 * Source label
 * @type {!string}
 * @export
 */
API.Client.PlayerSource.prototype.label;

/**
 * If Source is the default Source
 * @type {!boolean}
 * @export
 */
API.Client.PlayerSource.prototype._default;

/**
 * Sort order index
 * @type {!number}
 * @export
 */
API.Client.PlayerSource.prototype.order;

/**
 * @type {!number}
 * @export
 */
API.Client.PlayerSource.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.PlayerSource.prototype.playlistItemId;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.PlayerSource.prototype.playlistItem;

