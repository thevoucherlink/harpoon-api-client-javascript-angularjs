/**
 * @fileoverview AUTOMATICALLY GENERATED service for API.Client.PlayerSourceApi.
 * Do not edit this file by hand or your changes will be lost next time it is
 * generated.
 *
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at &lt;a href&#x3D;&#39;https://harpoonconnect.com&#39;&gt;https://harpoonconnect.com&lt;/a&gt;, #harpoonConnect.
 * Version: 1.1.1
 * Generated by: class io.swagger.codegen.languages.JavascriptClosureAngularClientCodegen
 */
goog.provide('API.Client.PlayerSourceApi');

goog.require('API.Client.PlayerSource');
goog.require('API.Client.PlaylistItem');
goog.require('API.Client.inline_response_200_1');
goog.require('API.Client.inline_response_200_2');
goog.require('API.Client.inline_response_200_3');

/**
 * @constructor
 * @param {!angular.$http} $http
 * @param {!Object} $httpParamSerializer
 * @param {!angular.$injector} $injector
 * @struct
 */
API.Client.PlayerSourceApi = function($http, $httpParamSerializer, $injector) {
  /** @private {!string} */
  this.basePath_ = $injector.has('PlayerSourceApiBasePath') ?
                   /** @type {!string} */ ($injector.get('PlayerSourceApiBasePath')) :
                   'https://apicdn.harpoonconnect.com/v1.1.1';

  /** @private {!Object<string, string>} */
  this.defaultHeaders_ = $injector.has('PlayerSourceApiDefaultHeaders') ?
                   /** @type {!Object<string, string>} */ (
                       $injector.get('PlayerSourceApiDefaultHeaders')) :
                   {};

  /** @private {!angular.$http} */
  this.http_ = $http;

  /** @package {!Object} */
  this.httpParamSerializer = $injector.get('$httpParamSerializer');
}
API.Client.PlayerSourceApi.$inject = ['$http', '$httpParamSerializer', '$injector'];

/**
 * Count instances of the model matched by where from the data source.
 * 
 * @param {!string=} opt_where Criteria to match model instances
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.inline_response_200_1>}
 */
API.Client.PlayerSourceApi.prototype.playerSourceCount = function(opt_where, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/PlayerSources/count';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  if (opt_where !== undefined) {
    queryParameters['where'] = opt_where;
  }

  /** @type {!Object} */
  var httpRequestParams = {
    method: 'GET',
    url: path,
    json: true,
            params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Create a new instance of the model and persist it into the data source.
 * 
 * @param {!PlayerSource=} opt_data Model instance data
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.PlayerSource>}
 */
API.Client.PlayerSourceApi.prototype.playerSourceCreate = function(opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/PlayerSources';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'POST',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Create a change stream.
 * 
 * @param {!string=} opt_options 
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!Object>}
 */
API.Client.PlayerSourceApi.prototype.playerSourceCreateChangeStreamGetPlayerSourcesChangeStream = function(opt_options, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/PlayerSources/change-stream';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  if (opt_options !== undefined) {
    queryParameters['options'] = opt_options;
  }

  /** @type {!Object} */
  var httpRequestParams = {
    method: 'GET',
    url: path,
    json: true,
            params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Create a change stream.
 * 
 * @param {!string=} opt_options 
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!Object>}
 */
API.Client.PlayerSourceApi.prototype.playerSourceCreateChangeStreamPostPlayerSourcesChangeStream = function(opt_options, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/PlayerSources/change-stream';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  /** @type {!Object} */
  var formParams = {};

  headerParams['Content-Type'] = 'application/x-www-form-urlencoded';

  formParams['options'] = opt_options;

  /** @type {!Object} */
  var httpRequestParams = {
    method: 'POST',
    url: path,
    json: false,
        data: this.httpParamSerializer(formParams),
    params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Delete a model instance by {{id}} from the data source.
 * 
 * @param {!string} id Model id
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.Object>}
 */
API.Client.PlayerSourceApi.prototype.playerSourceDeleteById = function(id, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/PlayerSources/{id}'
      .replace('{' + 'id' + '}', String(id));

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  // verify required parameter 'id' is set
  if (!id) {
    throw new Error('Missing required parameter id when calling playerSourceDeleteById');
  }
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'DELETE',
    url: path,
    json: true,
            params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Check whether a model instance exists in the data source.
 * 
 * @param {!string} id Model id
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.inline_response_200_3>}
 */
API.Client.PlayerSourceApi.prototype.playerSourceExistsGetPlayerSourcesidExists = function(id, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/PlayerSources/{id}/exists'
      .replace('{' + 'id' + '}', String(id));

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  // verify required parameter 'id' is set
  if (!id) {
    throw new Error('Missing required parameter id when calling playerSourceExistsGetPlayerSourcesidExists');
  }
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'GET',
    url: path,
    json: true,
            params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Check whether a model instance exists in the data source.
 * 
 * @param {!string} id Model id
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.inline_response_200_3>}
 */
API.Client.PlayerSourceApi.prototype.playerSourceExistsHeadPlayerSourcesid = function(id, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/PlayerSources/{id}'
      .replace('{' + 'id' + '}', String(id));

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  // verify required parameter 'id' is set
  if (!id) {
    throw new Error('Missing required parameter id when calling playerSourceExistsHeadPlayerSourcesid');
  }
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'HEAD',
    url: path,
    json: true,
            params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Find all instances of the model matched by filter from the data source.
 * 
 * @param {!string=} opt_filter Filter defining fields, where, include, order, offset, and limit
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!Array<!API.Client.PlayerSource>>}
 */
API.Client.PlayerSourceApi.prototype.playerSourceFind = function(opt_filter, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/PlayerSources';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  if (opt_filter !== undefined) {
    queryParameters['filter'] = opt_filter;
  }

  /** @type {!Object} */
  var httpRequestParams = {
    method: 'GET',
    url: path,
    json: true,
            params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Find a model instance by {{id}} from the data source.
 * 
 * @param {!string} id Model id
 * @param {!string=} opt_filter Filter defining fields and include
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.PlayerSource>}
 */
API.Client.PlayerSourceApi.prototype.playerSourceFindById = function(id, opt_filter, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/PlayerSources/{id}'
      .replace('{' + 'id' + '}', String(id));

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  // verify required parameter 'id' is set
  if (!id) {
    throw new Error('Missing required parameter id when calling playerSourceFindById');
  }
  if (opt_filter !== undefined) {
    queryParameters['filter'] = opt_filter;
  }

  /** @type {!Object} */
  var httpRequestParams = {
    method: 'GET',
    url: path,
    json: true,
            params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Find first instance of the model matched by filter from the data source.
 * 
 * @param {!string=} opt_filter Filter defining fields, where, include, order, offset, and limit
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.PlayerSource>}
 */
API.Client.PlayerSourceApi.prototype.playerSourceFindOne = function(opt_filter, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/PlayerSources/findOne';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  if (opt_filter !== undefined) {
    queryParameters['filter'] = opt_filter;
  }

  /** @type {!Object} */
  var httpRequestParams = {
    method: 'GET',
    url: path,
    json: true,
            params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Fetches belongsTo relation playlistItem.
 * 
 * @param {!string} id PlayerSource id
 * @param {!boolean=} opt_refresh 
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.PlaylistItem>}
 */
API.Client.PlayerSourceApi.prototype.playerSourcePrototypeGetPlaylistItem = function(id, opt_refresh, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/PlayerSources/{id}/playlistItem'
      .replace('{' + 'id' + '}', String(id));

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  // verify required parameter 'id' is set
  if (!id) {
    throw new Error('Missing required parameter id when calling playerSourcePrototypeGetPlaylistItem');
  }
  if (opt_refresh !== undefined) {
    queryParameters['refresh'] = opt_refresh;
  }

  /** @type {!Object} */
  var httpRequestParams = {
    method: 'GET',
    url: path,
    json: true,
            params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Patch attributes for a model instance and persist it into the data source.
 * 
 * @param {!string} id PlayerSource id
 * @param {!PlayerSource=} opt_data An object of model property name/value pairs
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.PlayerSource>}
 */
API.Client.PlayerSourceApi.prototype.playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid = function(id, opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/PlayerSources/{id}'
      .replace('{' + 'id' + '}', String(id));

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  // verify required parameter 'id' is set
  if (!id) {
    throw new Error('Missing required parameter id when calling playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid');
  }
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'PATCH',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Patch attributes for a model instance and persist it into the data source.
 * 
 * @param {!string} id PlayerSource id
 * @param {!PlayerSource=} opt_data An object of model property name/value pairs
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.PlayerSource>}
 */
API.Client.PlayerSourceApi.prototype.playerSourcePrototypeUpdateAttributesPutPlayerSourcesid = function(id, opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/PlayerSources/{id}'
      .replace('{' + 'id' + '}', String(id));

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  // verify required parameter 'id' is set
  if (!id) {
    throw new Error('Missing required parameter id when calling playerSourcePrototypeUpdateAttributesPutPlayerSourcesid');
  }
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'PUT',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Replace attributes for a model instance and persist it into the data source.
 * 
 * @param {!string} id Model id
 * @param {!PlayerSource=} opt_data Model instance data
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.PlayerSource>}
 */
API.Client.PlayerSourceApi.prototype.playerSourceReplaceById = function(id, opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/PlayerSources/{id}/replace'
      .replace('{' + 'id' + '}', String(id));

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  // verify required parameter 'id' is set
  if (!id) {
    throw new Error('Missing required parameter id when calling playerSourceReplaceById');
  }
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'POST',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Replace an existing model instance or insert a new one into the data source.
 * 
 * @param {!PlayerSource=} opt_data Model instance data
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.PlayerSource>}
 */
API.Client.PlayerSourceApi.prototype.playerSourceReplaceOrCreate = function(opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/PlayerSources/replaceOrCreate';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'POST',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Update instances of the model matched by {{where}} from the data source.
 * 
 * @param {!string=} opt_where Criteria to match model instances
 * @param {!PlayerSource=} opt_data An object of model property name/value pairs
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.inline_response_200_2>}
 */
API.Client.PlayerSourceApi.prototype.playerSourceUpdateAll = function(opt_where, opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/PlayerSources/update';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  if (opt_where !== undefined) {
    queryParameters['where'] = opt_where;
  }

  /** @type {!Object} */
  var httpRequestParams = {
    method: 'POST',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Patch an existing model instance or insert a new one into the data source.
 * 
 * @param {!PlayerSource=} opt_data Model instance data
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.PlayerSource>}
 */
API.Client.PlayerSourceApi.prototype.playerSourceUpsertPatchPlayerSources = function(opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/PlayerSources';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'PATCH',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Patch an existing model instance or insert a new one into the data source.
 * 
 * @param {!PlayerSource=} opt_data Model instance data
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.PlayerSource>}
 */
API.Client.PlayerSourceApi.prototype.playerSourceUpsertPutPlayerSources = function(opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/PlayerSources';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  /** @type {!Object} */
  var httpRequestParams = {
    method: 'PUT',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}

/**
 * Update an existing model instance or insert a new one into the data source based on the where criteria.
 * 
 * @param {!string=} opt_where Criteria to match model instances
 * @param {!PlayerSource=} opt_data An object of model property name/value pairs
 * @param {!angular.$http.Config=} opt_extraHttpRequestParams Extra HTTP parameters to send.
 * @return {!angular.$q.Promise<!API.Client.PlayerSource>}
 */
API.Client.PlayerSourceApi.prototype.playerSourceUpsertWithWhere = function(opt_where, opt_data, opt_extraHttpRequestParams) {
  /** @const {string} */
  var path = this.basePath_ + '/PlayerSources/upsertWithWhere';

  /** @type {!Object} */
  var queryParameters = {};

  /** @type {!Object} */
  var headerParams = angular.extend({}, this.defaultHeaders_);
  if (opt_where !== undefined) {
    queryParameters['where'] = opt_where;
  }

  /** @type {!Object} */
  var httpRequestParams = {
    method: 'POST',
    url: path,
    json: true,
    data: opt_data,
        params: queryParameters,
    headers: headerParams
  };

  if (opt_extraHttpRequestParams) {
    httpRequestParams = angular.extend(httpRequestParams, opt_extraHttpRequestParams);
  }

  return (/** @type {?} */ (this.http_))(httpRequestParams);
}
