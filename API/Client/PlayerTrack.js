goog.provide('API.Client.PlayerTrack');

/**
 * @record
 */
API.Client.PlayerTrack = function() {}

/**
 * Source file
 * @type {!string}
 * @export
 */
API.Client.PlayerTrack.prototype.file;

/**
 * Source type (used only in JS SDK)
 * @type {!string}
 * @export
 */
API.Client.PlayerTrack.prototype.type;

/**
 * Source label
 * @type {!string}
 * @export
 */
API.Client.PlayerTrack.prototype.label;

/**
 * If Source is the default Source
 * @type {!boolean}
 * @export
 */
API.Client.PlayerTrack.prototype._default;

/**
 * Sort order index
 * @type {!number}
 * @export
 */
API.Client.PlayerTrack.prototype.order;

/**
 * @type {!number}
 * @export
 */
API.Client.PlayerTrack.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.PlayerTrack.prototype.playlistItemId;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.PlayerTrack.prototype.playlistItem;

