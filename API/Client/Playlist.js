goog.provide('API.Client.Playlist');

/**
 * @record
 */
API.Client.Playlist = function() {}

/**
 * Playlist name
 * @type {!string}
 * @export
 */
API.Client.Playlist.prototype.name;

/**
 * Playlist short description
 * @type {!string}
 * @export
 */
API.Client.Playlist.prototype.shortDescription;

/**
 * Playlist image
 * @type {!string}
 * @export
 */
API.Client.Playlist.prototype.image;

/**
 * If Playlist is included in Main
 * @type {!boolean}
 * @export
 */
API.Client.Playlist.prototype.isMain;

/**
 * @type {!number}
 * @export
 */
API.Client.Playlist.prototype.id;

/**
 * @type {!string}
 * @export
 */
API.Client.Playlist.prototype.appId;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.Playlist.prototype.playlistItems;

