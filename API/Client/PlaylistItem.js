goog.provide('API.Client.PlaylistItem');

/**
 * @record
 */
API.Client.PlaylistItem = function() {}

/**
 * Stream title
 * @type {!string}
 * @export
 */
API.Client.PlaylistItem.prototype.title;

/**
 * Stream short description (on SDKs is under desc)
 * @type {!string}
 * @export
 */
API.Client.PlaylistItem.prototype.shortDescription;

/**
 * Playlist image
 * @type {!string}
 * @export
 */
API.Client.PlaylistItem.prototype.image;

/**
 * Stream file
 * @type {!string}
 * @export
 */
API.Client.PlaylistItem.prototype.file;

/**
 * Stream type (used only in JS SDK)
 * @type {!string}
 * @export
 */
API.Client.PlaylistItem.prototype.type;

/**
 * Stream media type (e.g. video , audio , radioStream)
 * @type {!string}
 * @export
 */
API.Client.PlaylistItem.prototype.mediaType;

/**
 * Ad media id (on SDKs is under mediaid)
 * @type {!number}
 * @export
 */
API.Client.PlaylistItem.prototype.mediaId;

/**
 * Sort order index
 * @type {!number}
 * @export
 */
API.Client.PlaylistItem.prototype.order;

/**
 * @type {!number}
 * @export
 */
API.Client.PlaylistItem.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.PlaylistItem.prototype.playlistId;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.PlaylistItem.prototype.playlist;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.PlaylistItem.prototype.playerSources;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.PlaylistItem.prototype.playerTracks;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.PlaylistItem.prototype.radioShows;

