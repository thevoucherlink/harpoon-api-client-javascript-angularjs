goog.provide('API.Client.Product');

/**
 * @record
 */
API.Client.Product = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.Product.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.Product.prototype.description;

/**
 * @type {!string}
 * @export
 */
API.Client.Product.prototype.cover;

/**
 * @type {!number}
 * @export
 */
API.Client.Product.prototype.price;

/**
 * @type {!string}
 * @export
 */
API.Client.Product.prototype.baseCurrency;

/**
 * @type {!number}
 * @export
 */
API.Client.Product.prototype.id;

