goog.provide('API.Client.PushWooshConfig');

/**
 * @record
 */
API.Client.PushWooshConfig = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.PushWooshConfig.prototype.appName;

/**
 * @type {!string}
 * @export
 */
API.Client.PushWooshConfig.prototype.appId;

/**
 * @type {!string}
 * @export
 */
API.Client.PushWooshConfig.prototype.appToken;

