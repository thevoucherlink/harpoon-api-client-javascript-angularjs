goog.provide('API.Client.RadioPlaySession');

/**
 * @record
 */
API.Client.RadioPlaySession = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.RadioPlaySession.prototype.sessionTime;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.RadioPlaySession.prototype.playUpdateTime;

/**
 * @type {!number}
 * @export
 */
API.Client.RadioPlaySession.prototype.radioStreamId;

/**
 * @type {!number}
 * @export
 */
API.Client.RadioPlaySession.prototype.radioShowId;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.RadioPlaySession.prototype.playEndTime;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.RadioPlaySession.prototype.playStartTime;

/**
 * @type {!number}
 * @export
 */
API.Client.RadioPlaySession.prototype.customerId;

/**
 * @type {!number}
 * @export
 */
API.Client.RadioPlaySession.prototype.id;

