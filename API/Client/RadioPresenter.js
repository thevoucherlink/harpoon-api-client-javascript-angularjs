goog.provide('API.Client.RadioPresenter');

/**
 * @record
 */
API.Client.RadioPresenter = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.RadioPresenter.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.RadioPresenter.prototype.image;

/**
 * Contacts for this presenter
 * @type {!API.Client.Contact}
 * @export
 */
API.Client.RadioPresenter.prototype.contact;

/**
 * @type {!number}
 * @export
 */
API.Client.RadioPresenter.prototype.id;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.RadioPresenter.prototype.radioShows;

