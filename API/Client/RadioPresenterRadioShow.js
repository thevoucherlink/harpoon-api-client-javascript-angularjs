goog.provide('API.Client.RadioPresenterRadioShow');

/**
 * @record
 */
API.Client.RadioPresenterRadioShow = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.RadioPresenterRadioShow.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.RadioPresenterRadioShow.prototype.radioPresenterId;

/**
 * @type {!number}
 * @export
 */
API.Client.RadioPresenterRadioShow.prototype.radioShowId;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.RadioPresenterRadioShow.prototype.radioPresenter;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.RadioPresenterRadioShow.prototype.radioShow;

