goog.provide('API.Client.RadioShow');

/**
 * @record
 */
API.Client.RadioShow = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.RadioShow.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.RadioShow.prototype.description;

/**
 * @type {!string}
 * @export
 */
API.Client.RadioShow.prototype.content;

/**
 * @type {!string}
 * @export
 */
API.Client.RadioShow.prototype.contentType;

/**
 * Contacts for this show
 * @type {!API.Client.Contact}
 * @export
 */
API.Client.RadioShow.prototype.contact;

/**
 * When the Show starts of being public
 * @type {!API.Client.date}
 * @export
 */
API.Client.RadioShow.prototype.starts;

/**
 * When the Show ceases of being public
 * @type {!API.Client.date}
 * @export
 */
API.Client.RadioShow.prototype.ends;

/**
 * @type {!string}
 * @export
 */
API.Client.RadioShow.prototype.type;

/**
 * @type {!API.Client.RadioShowTime}
 * @export
 */
API.Client.RadioShow.prototype.radioShowTimeCurrent;

/**
 * @type {!string}
 * @export
 */
API.Client.RadioShow.prototype.imgShow;

/**
 * Url of the sponsor MP3 to be played
 * @type {!string}
 * @export
 */
API.Client.RadioShow.prototype.sponsorTrack;

/**
 * @type {!number}
 * @export
 */
API.Client.RadioShow.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.RadioShow.prototype.radioStreamId;

/**
 * @type {!number}
 * @export
 */
API.Client.RadioShow.prototype.playlistItemId;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.RadioShow.prototype.radioPresenters;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.RadioShow.prototype.radioStream;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.RadioShow.prototype.radioShowTimes;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.RadioShow.prototype.playlistItem;

/** @enum {string} */
API.Client.RadioShow.ContentTypeEnum = { 
  image: 'image',
  video: 'video',
}
