goog.provide('API.Client.RadioShowTime');

/**
 * @record
 */
API.Client.RadioShowTime = function() {}

/**
 * Time when the show starts, format HHmm
 * @type {!string}
 * @export
 */
API.Client.RadioShowTime.prototype.startTime;

/**
 * Time when the show ends, format HHmm
 * @type {!string}
 * @export
 */
API.Client.RadioShowTime.prototype.endTime;

/**
 * Day of the week when the show is live, 1 = Monday / 7 = Sunday
 * @type {!number}
 * @export
 */
API.Client.RadioShowTime.prototype.weekday;

/**
 * @type {!number}
 * @export
 */
API.Client.RadioShowTime.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.RadioShowTime.prototype.radioShowId;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.RadioShowTime.prototype.radioShow;

