goog.provide('API.Client.RadioStream');

/**
 * @record
 */
API.Client.RadioStream = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.RadioStream.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.RadioStream.prototype.description;

/**
 * @type {!string}
 * @export
 */
API.Client.RadioStream.prototype.urlHQ;

/**
 * @type {!string}
 * @export
 */
API.Client.RadioStream.prototype.urlLQ;

/**
 * When the Stream starts of being public
 * @type {!API.Client.date}
 * @export
 */
API.Client.RadioStream.prototype.starts;

/**
 * When the Stream ceases of being public
 * @type {!API.Client.date}
 * @export
 */
API.Client.RadioStream.prototype.ends;

/**
 * @type {!string}
 * @export
 */
API.Client.RadioStream.prototype.metaDataUrl;

/**
 * @type {!string}
 * @export
 */
API.Client.RadioStream.prototype.imgStream;

/**
 * Url of the sponsor MP3 to be played
 * @type {!string}
 * @export
 */
API.Client.RadioStream.prototype.sponsorTrack;

/**
 * @type {!API.Client.TritonConfig}
 * @export
 */
API.Client.RadioStream.prototype.tritonConfig;

/**
 * @type {!number}
 * @export
 */
API.Client.RadioStream.prototype.id;

/**
 * @type {!string}
 * @export
 */
API.Client.RadioStream.prototype.appId;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.RadioStream.prototype.radioShows;

