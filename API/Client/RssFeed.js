goog.provide('API.Client.RssFeed');

/**
 * @record
 */
API.Client.RssFeed = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.RssFeed.prototype.category;

/**
 * @type {!string}
 * @export
 */
API.Client.RssFeed.prototype.subCategory;

/**
 * @type {!string}
 * @export
 */
API.Client.RssFeed.prototype.url;

/**
 * @type {!string}
 * @export
 */
API.Client.RssFeed.prototype.name;

/**
 * @type {!number}
 * @export
 */
API.Client.RssFeed.prototype.sortOrder;

/**
 * @type {!string}
 * @export
 */
API.Client.RssFeed.prototype.styleCss;

/**
 * @type {!boolean}
 * @export
 */
API.Client.RssFeed.prototype.noAds;

/**
 * @type {!boolean}
 * @export
 */
API.Client.RssFeed.prototype.adMRectVisible;

/**
 * @type {!number}
 * @export
 */
API.Client.RssFeed.prototype.adMRectPosition;

/**
 * @type {!number}
 * @export
 */
API.Client.RssFeed.prototype.id;

/**
 * @type {!string}
 * @export
 */
API.Client.RssFeed.prototype.appId;

/**
 * @type {!number}
 * @export
 */
API.Client.RssFeed.prototype.rssFeedGroupId;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.RssFeed.prototype.rssFeedGroup;

