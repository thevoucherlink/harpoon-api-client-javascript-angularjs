goog.provide('API.Client.RssFeedGroup');

/**
 * @record
 */
API.Client.RssFeedGroup = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.RssFeedGroup.prototype.name;

/**
 * @type {!number}
 * @export
 */
API.Client.RssFeedGroup.prototype.sortOrder;

/**
 * @type {!number}
 * @export
 */
API.Client.RssFeedGroup.prototype.id;

/**
 * @type {!string}
 * @export
 */
API.Client.RssFeedGroup.prototype.appId;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.RssFeedGroup.prototype.rssFeeds;

