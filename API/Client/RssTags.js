goog.provide('API.Client.RssTags');

/**
 * @record
 */
API.Client.RssTags = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.RssTags.prototype.imgTumbnail;

/**
 * @type {!string}
 * @export
 */
API.Client.RssTags.prototype.imgMain;

/**
 * @type {!string}
 * @export
 */
API.Client.RssTags.prototype.link;

/**
 * @type {!string}
 * @export
 */
API.Client.RssTags.prototype.postDateTime;

/**
 * @type {!string}
 * @export
 */
API.Client.RssTags.prototype.description;

/**
 * @type {!string}
 * @export
 */
API.Client.RssTags.prototype.category;

/**
 * @type {!string}
 * @export
 */
API.Client.RssTags.prototype.title;

/**
 * @type {!number}
 * @export
 */
API.Client.RssTags.prototype.id;

