goog.provide('API.Client.StripeCoupon');

/**
 * @record
 */
API.Client.StripeCoupon = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.StripeCoupon.prototype.object;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeCoupon.prototype.amountOff;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeCoupon.prototype.created;

/**
 * @type {!string}
 * @export
 */
API.Client.StripeCoupon.prototype.currency;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeCoupon.prototype.durationInMonths;

/**
 * @type {!boolean}
 * @export
 */
API.Client.StripeCoupon.prototype.livemode;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeCoupon.prototype.maxRedemptions;

/**
 * @type {!string}
 * @export
 */
API.Client.StripeCoupon.prototype.metadata;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeCoupon.prototype.percentOff;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeCoupon.prototype.redeemBy;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeCoupon.prototype.timesRedeemed;

/**
 * @type {!boolean}
 * @export
 */
API.Client.StripeCoupon.prototype.valid;

/**
 * @type {!string}
 * @export
 */
API.Client.StripeCoupon.prototype.duration;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeCoupon.prototype.id;

