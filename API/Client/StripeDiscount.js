goog.provide('API.Client.StripeDiscount');

/**
 * @record
 */
API.Client.StripeDiscount = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.StripeDiscount.prototype.object;

/**
 * @type {!string}
 * @export
 */
API.Client.StripeDiscount.prototype.customer;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeDiscount.prototype.end;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeDiscount.prototype.start;

/**
 * @type {!string}
 * @export
 */
API.Client.StripeDiscount.prototype.subscription;

/**
 * @type {!API.Client.StripeCoupon}
 * @export
 */
API.Client.StripeDiscount.prototype.coupon;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeDiscount.prototype.id;

