goog.provide('API.Client.StripeInvoice');

/**
 * @record
 */
API.Client.StripeInvoice = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.StripeInvoice.prototype.object;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeInvoice.prototype.amountDue;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeInvoice.prototype.applicationFee;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeInvoice.prototype.attemptCount;

/**
 * @type {!boolean}
 * @export
 */
API.Client.StripeInvoice.prototype.attempted;

/**
 * @type {!string}
 * @export
 */
API.Client.StripeInvoice.prototype.charge;

/**
 * @type {!boolean}
 * @export
 */
API.Client.StripeInvoice.prototype.closed;

/**
 * @type {!string}
 * @export
 */
API.Client.StripeInvoice.prototype.currency;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeInvoice.prototype.date;

/**
 * @type {!string}
 * @export
 */
API.Client.StripeInvoice.prototype.description;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeInvoice.prototype.endingBalance;

/**
 * @type {!boolean}
 * @export
 */
API.Client.StripeInvoice.prototype.forgiven;

/**
 * @type {!boolean}
 * @export
 */
API.Client.StripeInvoice.prototype.livemode;

/**
 * @type {!string}
 * @export
 */
API.Client.StripeInvoice.prototype.metadata;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeInvoice.prototype.nextPaymentAttempt;

/**
 * @type {!boolean}
 * @export
 */
API.Client.StripeInvoice.prototype.paid;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeInvoice.prototype.periodEnd;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeInvoice.prototype.periodStart;

/**
 * @type {!string}
 * @export
 */
API.Client.StripeInvoice.prototype.receiptNumber;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeInvoice.prototype.subscriptionProrationDate;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeInvoice.prototype.subtotal;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeInvoice.prototype.tax;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeInvoice.prototype.taxPercent;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeInvoice.prototype.total;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeInvoice.prototype.webhooksDeliveredAt;

/**
 * @type {!string}
 * @export
 */
API.Client.StripeInvoice.prototype.customer;

/**
 * @type {!API.Client.StripeDiscount}
 * @export
 */
API.Client.StripeInvoice.prototype.discount;

/**
 * @type {!string}
 * @export
 */
API.Client.StripeInvoice.prototype.statmentDescriptor;

/**
 * @type {!API.Client.StripeSubscription}
 * @export
 */
API.Client.StripeInvoice.prototype.subscription;

/**
 * @type {!Array<!API.Client.StripeInvoiceItem>}
 * @export
 */
API.Client.StripeInvoice.prototype.lines;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeInvoice.prototype.id;

