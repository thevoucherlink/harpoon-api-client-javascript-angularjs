goog.provide('API.Client.StripeInvoiceItem');

/**
 * @record
 */
API.Client.StripeInvoiceItem = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.StripeInvoiceItem.prototype.object;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeInvoiceItem.prototype.id;

