goog.provide('API.Client.StripePlan');

/**
 * @record
 */
API.Client.StripePlan = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.StripePlan.prototype.amount;

/**
 * @type {!string}
 * @export
 */
API.Client.StripePlan.prototype.currency;

/**
 * @type {!string}
 * @export
 */
API.Client.StripePlan.prototype.interval;

/**
 * @type {!number}
 * @export
 */
API.Client.StripePlan.prototype.intervalCount;

/**
 * @type {!string}
 * @export
 */
API.Client.StripePlan.prototype.metadata;

/**
 * @type {!string}
 * @export
 */
API.Client.StripePlan.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.StripePlan.prototype.statementDescriptor;

/**
 * @type {!number}
 * @export
 */
API.Client.StripePlan.prototype.trialPeriodDays;

/**
 * @type {!number}
 * @export
 */
API.Client.StripePlan.prototype.id;

