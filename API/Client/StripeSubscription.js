goog.provide('API.Client.StripeSubscription');

/**
 * @record
 */
API.Client.StripeSubscription = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.StripeSubscription.prototype.applicationFeePercent;

/**
 * @type {!boolean}
 * @export
 */
API.Client.StripeSubscription.prototype.cancelAtPeriodEnd;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeSubscription.prototype.canceledAt;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeSubscription.prototype.currentPeriodEnd;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeSubscription.prototype.currentPeriodStart;

/**
 * @type {!string}
 * @export
 */
API.Client.StripeSubscription.prototype.metadata;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeSubscription.prototype.quantity;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeSubscription.prototype.start;

/**
 * @type {!string}
 * @export
 */
API.Client.StripeSubscription.prototype.status;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeSubscription.prototype.taxPercent;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeSubscription.prototype.trialEnd;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeSubscription.prototype.trialStart;

/**
 * @type {!string}
 * @export
 */
API.Client.StripeSubscription.prototype.customer;

/**
 * @type {!API.Client.StripeDiscount}
 * @export
 */
API.Client.StripeSubscription.prototype.discount;

/**
 * @type {!API.Client.StripePlan}
 * @export
 */
API.Client.StripeSubscription.prototype.plan;

/**
 * @type {!string}
 * @export
 */
API.Client.StripeSubscription.prototype.object;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeSubscription.prototype.endedAt;

/**
 * @type {!number}
 * @export
 */
API.Client.StripeSubscription.prototype.id;

