goog.provide('API.Client.TritonConfig');

/**
 * @record
 */
API.Client.TritonConfig = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.TritonConfig.prototype.stationId;

/**
 * @type {!string}
 * @export
 */
API.Client.TritonConfig.prototype.stationName;

/**
 * @type {!string}
 * @export
 */
API.Client.TritonConfig.prototype.mp3Mount;

/**
 * @type {!string}
 * @export
 */
API.Client.TritonConfig.prototype.aacMount;

