goog.provide('API.Client.TwitterConfig');

/**
 * @record
 */
API.Client.TwitterConfig = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.TwitterConfig.prototype.apiKey;

/**
 * @type {!string}
 * @export
 */
API.Client.TwitterConfig.prototype.apiSecret;

/**
 * @type {!number}
 * @export
 */
API.Client.TwitterConfig.prototype.id;

