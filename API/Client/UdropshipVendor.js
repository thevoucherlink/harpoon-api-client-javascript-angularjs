goog.provide('API.Client.UdropshipVendor');

/**
 * @record
 */
API.Client.UdropshipVendor = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.id;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.vendorName;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.vendorAttn;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.email;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.street;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.city;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.zip;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.countryId;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.regionId;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.region;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.telephone;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.fax;

/**
 * @type {!boolean}
 * @export
 */
API.Client.UdropshipVendor.prototype.status;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.password;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.passwordHash;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.passwordEnc;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.carrierCode;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.notifyNewOrder;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.labelType;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.testMode;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.handlingFee;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.upsShipperNumber;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.customDataCombined;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.customVarsCombined;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.emailTemplate;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.urlKey;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.randomHash;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.UdropshipVendor.prototype.createdAt;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.notifyLowstock;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.notifyLowstockQty;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.useHandlingFee;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.useRatesFallback;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.allowShippingExtraCharge;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.defaultShippingExtraChargeSuffix;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.defaultShippingExtraChargeType;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.defaultShippingExtraCharge;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.isExtraChargeShippingDefault;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.defaultShippingId;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.billingUseShipping;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.billingEmail;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.billingTelephone;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.billingFax;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.billingVendorAttn;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.billingStreet;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.billingCity;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.billingZip;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.billingCountryId;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.billingRegionId;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.billingRegion;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.subdomainLevel;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.updateStoreBaseUrl;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.confirmation;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.confirmationSent;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.rejectReason;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.backorderByAvailability;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.useReservedQty;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.tiercomRates;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.tiercomFixedRule;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.tiercomFixedRates;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.tiercomFixedCalcType;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.tiershipRates;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.tiershipSimpleRates;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.tiershipUseV2Rates;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.vacationMode;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.UdropshipVendor.prototype.vacationEnd;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.vacationMessage;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.udmemberLimitProducts;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.udmemberProfileId;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.udmemberProfileRefid;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.udmemberMembershipCode;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.udmemberMembershipTitle;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.udmemberBillingType;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.udmemberHistory;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.udmemberProfileSyncOff;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendor.prototype.udmemberAllowMicrosite;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.udprodTemplateSku;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendor.prototype.vendorTaxClass;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.UdropshipVendor.prototype.catalogProducts;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.UdropshipVendor.prototype.vendorLocations;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.UdropshipVendor.prototype.config;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.UdropshipVendor.prototype.partners;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.UdropshipVendor.prototype.udropshipVendorProducts;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.UdropshipVendor.prototype.harpoonHpublicApplicationpartners;

/**
 * @type {!Array<!API.Client.Object>}
 * @export
 */
API.Client.UdropshipVendor.prototype.harpoonHpublicv12VendorAppCategories;

