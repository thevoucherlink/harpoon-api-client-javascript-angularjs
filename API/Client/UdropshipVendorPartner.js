goog.provide('API.Client.UdropshipVendorPartner');

/**
 * @record
 */
API.Client.UdropshipVendorPartner = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.status;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.invitedEmail;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.invitedAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.acceptedAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.createdAt;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.updatedAt;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.configList;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.configFeed;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.configNeedFollow;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.configAcceptEvent;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.configAcceptCoupon;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.configAcceptDealsimple;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.configAcceptDealgroup;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.configAcceptNotificationpush;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.configAcceptNotificationbeacon;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.isOwner;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.vendorId;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.partnerId;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.id;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.UdropshipVendorPartner.prototype.udropshipVendor;

