goog.provide('API.Client.UdropshipVendorProduct');

/**
 * @record
 */
API.Client.UdropshipVendorProduct = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorProduct.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorProduct.prototype.vendorId;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorProduct.prototype.productId;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorProduct.prototype.priority;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendorProduct.prototype.carrierCode;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendorProduct.prototype.vendorSku;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendorProduct.prototype.vendorCost;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendorProduct.prototype.stockQty;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorProduct.prototype.backorders;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendorProduct.prototype.shippingPrice;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorProduct.prototype.status;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendorProduct.prototype.reservedQty;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendorProduct.prototype.availState;

/**
 * @type {!API.Client.date}
 * @export
 */
API.Client.UdropshipVendorProduct.prototype.availDate;

/**
 * @type {!string}
 * @export
 */
API.Client.UdropshipVendorProduct.prototype.relationshipStatus;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.UdropshipVendorProduct.prototype.udropshipVendor;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.UdropshipVendorProduct.prototype.catalogProduct;

