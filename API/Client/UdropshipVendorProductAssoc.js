goog.provide('API.Client.UdropshipVendorProductAssoc');

/**
 * @record
 */
API.Client.UdropshipVendorProductAssoc = function() {}

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorProductAssoc.prototype.isUdmulti;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorProductAssoc.prototype.id;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorProductAssoc.prototype.isAttribute;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorProductAssoc.prototype.productId;

/**
 * @type {!number}
 * @export
 */
API.Client.UdropshipVendorProductAssoc.prototype.vendorId;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.UdropshipVendorProductAssoc.prototype.udropshipVendor;

/**
 * @type {!API.Client.Object}
 * @export
 */
API.Client.UdropshipVendorProductAssoc.prototype.catalogProduct;

