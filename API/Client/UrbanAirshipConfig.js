goog.provide('API.Client.UrbanAirshipConfig');

/**
 * @record
 */
API.Client.UrbanAirshipConfig = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.UrbanAirshipConfig.prototype.developmentAppKey;

/**
 * @type {!string}
 * @export
 */
API.Client.UrbanAirshipConfig.prototype.developmentAppSecret;

/**
 * @type {!string}
 * @export
 */
API.Client.UrbanAirshipConfig.prototype.developmentLogLevel;

/**
 * @type {!string}
 * @export
 */
API.Client.UrbanAirshipConfig.prototype.productionAppKey;

/**
 * @type {!string}
 * @export
 */
API.Client.UrbanAirshipConfig.prototype.productionAppSecret;

/**
 * @type {!string}
 * @export
 */
API.Client.UrbanAirshipConfig.prototype.productionLogLevel;

/**
 * @type {!boolean}
 * @export
 */
API.Client.UrbanAirshipConfig.prototype.inProduction;

/** @enum {string} */
API.Client.UrbanAirshipConfig.DevelopmentLogLevelEnum = { 
  undefined: 'undefined',
  none: 'none',
  error: 'error',
  warn: 'warn',
  info: 'info',
  debug: 'debug',
  trace: 'trace',
}
/** @enum {string} */
API.Client.UrbanAirshipConfig.ProductionLogLevelEnum = { 
  undefined: 'undefined',
  none: 'none',
  error: 'error',
  warn: 'warn',
  info: 'info',
  debug: 'debug',
  trace: 'trace',
}
