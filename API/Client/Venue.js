goog.provide('API.Client.Venue');

/**
 * @record
 */
API.Client.Venue = function() {}

/**
 * @type {!string}
 * @export
 */
API.Client.Venue.prototype.name;

/**
 * @type {!string}
 * @export
 */
API.Client.Venue.prototype.address;

/**
 * @type {!string}
 * @export
 */
API.Client.Venue.prototype.city;

/**
 * @type {!string}
 * @export
 */
API.Client.Venue.prototype.country;

/**
 * @type {!API.Client.GeoLocation}
 * @export
 */
API.Client.Venue.prototype.coordinates;

/**
 * @type {!string}
 * @export
 */
API.Client.Venue.prototype.phone;

/**
 * @type {!string}
 * @export
 */
API.Client.Venue.prototype.email;

/**
 * @type {!API.Client.Brand}
 * @export
 */
API.Client.Venue.prototype.brand;

/**
 * @type {!number}
 * @export
 */
API.Client.Venue.prototype.id;

